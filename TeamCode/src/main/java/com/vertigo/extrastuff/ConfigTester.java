package com.vertigo.extrastuff;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

import java.util.jar.Attributes;



@TeleOp(name="Configuration Tester", group="Utilities")
@Disabled
public class ConfigTester extends LinearOpMode {
    String[] buttonMap={"a","b","x","y"};
    public int isDepressed(int m){
        if (gamepad1.a && m==0){
            return 1;
        }
        if (gamepad1.b && m==3){
            return 1;
        }
        if (gamepad1.y && m==2){
            return 1;
        }
        if (gamepad1.x &&m==3){
            return 1;
        }
        return 0;
    }

    public void runOpMode(){
        hardwareMap.get(DcMotor.class, "motor0Hub2").setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();
        while (opModeIsActive()){
            for (int i = 0; i < 4; i++) {
                hardwareMap.get(DcMotor.class, "motor"+i+"Hub1").setPower(isDepressed(i));
            }
            hardwareMap.get(DcMotor.class, "motor0Hub2").setPower(gamepad1.left_stick_y);
            telemetry.addData("motor0Hub2encoder",hardwareMap.get(DcMotor.class, "motor0Hub2").getCurrentPosition());
            hardwareMap.get(DcMotor.class, "motor1Hub2").setPower(gamepad1.right_stick_y);
            telemetry.addData("motor1Hub2encoder",hardwareMap.get(DcMotor.class, "motor1Hub2").getCurrentPosition());
            hardwareMap.get(DcMotor.class, "motor2Hub2").setPower(gamepad1.left_trigger);
            telemetry.addData("motor2Hub2encoder",hardwareMap.get(DcMotor.class, "motor2Hub2").getCurrentPosition());
            hardwareMap.get(DcMotor.class, "motor3Hub2").setPower(gamepad1.right_trigger);
            telemetry.addData("motor3Hub2encoder",hardwareMap.get(DcMotor.class, "motor3Hub2").getCurrentPosition());
            telemetry.update();



        }
    }
}

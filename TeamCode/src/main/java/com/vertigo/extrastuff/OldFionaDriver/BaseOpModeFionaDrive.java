package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.vertigo.extrastuff.CenaPlayer;

import org.firstinspires.ftc.teamcode.NonOpModes.ControllerInput;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.disnodeteam.dogecv.Dogeforia;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

import java.util.ArrayList;
import java.util.List;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.XYZ;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.BACK;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.FRONT;



public abstract class BaseOpModeFionaDrive extends LinearOpMode {

    private GoldAlignDetector detector;
    private DcMotor leftDrive;
    private DcMotor rightDrive;

    private DcMotor leftLift;
    private DcMotor rightLift;
    private VertigoGyro gyro;
    private ColorSensor liftColorSensor;
    private Servo marker;
    private double markerDown=0.85;
    private double markerDrop=0.5;
    private ControllerInput controllerInput;
    //private WebcamName webcamName;
    //private Dogeforia vuforia;
    public GoldBlockPosition position;

    //Vuforia stuffs
    // Setup variables
    private ElapsedTime runtime = new ElapsedTime();
    private static final float mmPerInch        = 25.4f;
    private static final float mmFTCFieldWidth  = (12*6) * mmPerInch;       // the width of the FTC field (from the center point to the outer panels)
    private static final float mmTargetHeight   = (6) * mmPerInch;          // the height of the center of the target image above the floor

    // Select which camera you want use.  The FRONT camera is the one on the same side as the screen.
    // Valid choices are:  BACK or FRONT
    private static final VuforiaLocalizer.CameraDirection CAMERA_CHOICE = BACK;

    // Vuforia variables
    private OpenGLMatrix lastLocation = null;
    boolean targetVisible;
    Dogeforia vuforia;
    WebcamName webcamName;
    List<VuforiaTrackable> allTrackables = new ArrayList<VuforiaTrackable>();



    public VertigoTankDrive driveTrain;

    public enum OpModeType{
        AUTONOMOUS,TELEOP
        //Just in case we need to differentiate how we initialize the robot
    }
    public void runOpMode(OpModeType type){
        //First, init code for opencv stuff, IFF autonomous

        //Then, get the motors and gyro and make the drivetrain

        if (type == OpModeType.AUTONOMOUS){
            position = findBlock();
            telemetry.addData("Gold Position on init: ",position);
            telemetry.update();
            sleep(2000);
        }

        leftDrive  = hardwareMap.get(DcMotor.class, "leftDrive");
        rightDrive = hardwareMap.get(DcMotor.class, "rightDrive");

        leftLift = hardwareMap.get(DcMotor.class, "leftLift");
        rightLift = hardwareMap.get(DcMotor.class, "rightLift");
        leftLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        liftColorSensor = hardwareMap.get(ColorSensor.class, "liftColorSensor");


        marker = hardwareMap.servo.get("marker");
        if (type==OpModeType.AUTONOMOUS){
            //marker.setDirection(Servo.Direction.FORWARD);
            marker.setPosition(markerDown);
           raiseToLander();
        }

        controllerInput = new ControllerInput(gamepad1,telemetry);

        driveTrain = new VertigoTankDrive(leftDrive,rightDrive,hardwareMap.get(BNO055IMU.class, "imu"),telemetry);



        waitForStart();
        /*if (type==OpModeType.AUTONOMOUS){
            smokingTheseMeats();
        }*/

    }

    //19 ticks per inch
    public void smokingTheseMeats(){
        CenaPlayer.start(hardwareMap.appContext);
    }

    public void raiseToLander() {
        double runtimeTarget = getRuntime() + 6.5;
        while (getRuntime() < runtimeTarget) {
            driveLift(-1);
        }
        driveLift(0);
    }

        public void liftRobotUp(){
        leftLift.setPower(1);
        rightLift.setPower(-1);
    }

    public void startLookingForBlock(){
        detector = new GoldAlignDetector();
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        detector.useDefaults();

        // Optional Tuning
        detector.alignSize = 100; // How wide (in pixels) is the range in which the gold object will be aligned. (Represented by green bars in the preview)
        detector.alignPosOffset = 0; // How far from center frame to offset this alignment zone.
        detector.downscale = 0.4; // How much to downscale the input frames

        detector.areaScoringMethod = DogeCV.AreaScoringMethod.MAX_AREA; // Can also be PERFECT_AREA
        //detector.perfectAreaScorer.perfectArea = 10000; // if using PERFECT_AREA scoring
        detector.maxAreaScorer.weight = 0.005;

        detector.ratioScorer.weight = 5;
        detector.ratioScorer.perfectRatio = 1.0;

        detector.enable();
    }




    public void stopLookingForBlock(){
        detector.disable();
    }

    public void stopVuforiaLookingForBlock(){
        vuforia.stop();
    }

    public void driveLift(double power) {
        int red = hardwareMap.get(ColorSensor.class, "liftColorSensor").red();
        int blue = hardwareMap.get(ColorSensor.class, "liftColorSensor").blue();
        if (red < 1500 && power < 0) { //at blue, don't go down
            leftLift.setPower(0);
            rightLift.setPower(0);
        } else if (blue < 1600 && power > 0) {
            leftLift.setPower(0);
            rightLift.setPower(0);
        } else {
            leftLift.setPower(power);
            rightLift.setPower(power);
        }

    }


    public void dropMarker(){
        //getMarker().setDirection(Servo.Direction.FORWARD);
        getMarker().setPosition(markerDrop);
        sleep(1000);
        //getMarker().setDirection(Servo.Direction.REVERSE);
        getMarker().setPosition(markerDown);
    }
    public void dropFromLander(){
        double runtimeTarget = getRuntime()+6.5;
        while (getRuntime()<runtimeTarget){
            driveLift(1);
        }
        driveLift(0);

    }

    public void stopMakingNoise(){
        CenaPlayer.stop();
    }

    public GoldAlignDetector getDetector() {
        return detector;
    }

    public GoldBlockPosition findBlock(){
        startLookingForBlock();
        sleep(2000);
        if (getDetector().isFound()){
            if (getDetector().getXPosition()<300){
                stopLookingForBlock();
                return GoldBlockPosition.CENTER;
            }
            stopLookingForBlock();
            return GoldBlockPosition.RIGHT;
        }
        stopLookingForBlock();
        return GoldBlockPosition.LEFT;
    }

    enum GoldBlockPosition{
        LEFT,RIGHT,CENTER;
    }

    public void goForward  (){

    }

    public Servo getMarker() {
        return marker;
    }
    public void yeetThatGoldBoi(GoldBlockPosition position, boolean returnToPosition){
        driveTrain.driveForwardDistInches(-7);
        if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(33,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-40,0.5);

        } else if (position==GoldBlockPosition.RIGHT){
            driveTrain.turnDegrees(335,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(-42,0.5);

        } else if (position==GoldBlockPosition.CENTER){
            driveTrain.driveForwardDistInches(-60,0.75);

        }
    }

    public void yeetThatDoubleGoldBoi(GoldBlockPosition position){
        yeetThatGoldBoi(position);
        returnHome(position);
        craterToDepot();
        sleep(300);
        if (position==GoldBlockPosition.CENTER){
            driveTrain.turnDegrees(312,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(20,.5);
            driveTrain.driveForwardDistInches(-20,.5);
            driveTrain.turnDegrees(58,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(73);
        } else if (position==GoldBlockPosition.RIGHT){
            driveTrain.turnDegrees(340,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(24,.5);
            driveTrain.driveForwardDistInches(-24,.5);
            driveTrain.turnDegrees(20,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(73);

        } else if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(273,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(37,.5);
            //driveTrain.driveForwardDistInches(-37,.5);
            //driveTrain.turnDegrees(85,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-8,.5);
            driveTrain.turnDegrees(347,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(46);


        }



    }

    public void yeetThatGoldBoi(GoldBlockPosition position){
        driveTrain.driveForwardDistInches(-10);
        if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(40,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-29.5,0.5);
        }
        else if (position==GoldBlockPosition.RIGHT){
        driveTrain.turnDegrees(323,VertigoTankDrive.TurningDirection.RIGHT);
        driveTrain.driveForwardDistInches(-22,0.5);
        }
        else if (position==GoldBlockPosition.CENTER) {
            driveTrain.driveForwardDistInches(-19, 0.5);
        }

    }
    public void yeetThisGoldBoi(GoldBlockPosition position){
        driveTrain.driveForwardDistInches(-4);
        if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(40,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-28,0.5);
        }
        else if (position==GoldBlockPosition.RIGHT){
            driveTrain.turnDegrees(323,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(-22,0.5);
        }
        else if (position==GoldBlockPosition.CENTER) {
            driveTrain.driveForwardDistInches(-19, 0.5);
        }

    }

    public void craterToDepot(){
        driveTrain.driveForwardDistInches(-9.5,.5);
        driveTrain.turnDegrees(70,VertigoTankDrive.TurningDirection.LEFT);
        driveTrain.driveForwardDistInches(-57,.5);
        driveTrain.turnDegrees(34,VertigoTankDrive.TurningDirection.LEFT);
        driveTrain.driveForwardDistInches(-47,.5);
        dropMarker();
    }

    public void returnHome(GoldBlockPosition position){
        if (position==GoldBlockPosition.LEFT){
            driveTrain.driveForwardDistInches(20.5,0.5);
            driveTrain.turnDegrees(320,VertigoTankDrive.TurningDirection.RIGHT);
        } else if (position==GoldBlockPosition.RIGHT){
            driveTrain.driveForwardDistInches(22,0.5);
            driveTrain.turnDegrees(41,VertigoTankDrive.TurningDirection.LEFT);
        } else if (position==GoldBlockPosition.CENTER){
            driveTrain.driveForwardDistInches(20, .5);
        }
        sleep(300);
    }
    public void yeetTheMarker(GoldBlockPosition position){
        if (position==GoldBlockPosition.LEFT){
            driveTrain.driveForwardDistInches(-18,.5);
            driveTrain.driveForwardDistInches(5,.5);
            driveTrain.turnDegrees(270,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(-36,.5);
            dropMarker();
            driveTrain.turnDegrees(340,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(66);
            driveTrain.driveForwardDistInches(50,.25);
        } else if (position==GoldBlockPosition.RIGHT){
            driveTrain.driveForwardDistInches(-27,.5);
            driveTrain.driveForwardDistInches(2,.5);
            driveTrain.turnDegrees(79,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-40,.5);
            driveTrain.turnDegrees(270,VertigoTankDrive.TurningDirection.RIGHT);
            dropMarker();
            driveTrain.driveForwardDistInches(-5,.5);
            driveTrain.turnDegrees(342,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(10);
            driveTrain.turnDegrees(15,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(55);
            driveTrain.driveForwardDistInches(25,.25);
        } else if(position==GoldBlockPosition.CENTER){
            driveTrain.driveForwardDistInches(-35,.5);
            dropMarker();
            driveTrain.turnDegrees(300,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(20);
            driveTrain.turnDegrees(5,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(46);
            driveTrain.driveForwardDistInches(50,.25);

        }

    }

    public void runToCrater(){

        driveTrain.turnDegrees(5,VertigoTankDrive.TurningDirection.LEFT);
        driveTrain.driveForwardDistInches(60);
        driveTrain.driveForwardDistInches(25,.25);



    }

    public void yeetThyGoldBoi(GoldBlockPosition position){
        driveTrain.driveForwardDistInches(-10);
        if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(40,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-34,0.75);
        }
        else if (position==GoldBlockPosition.RIGHT){
            driveTrain.turnDegrees(327,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(-31,0.75);
        }
        else if (position==GoldBlockPosition.CENTER) {
            driveTrain.driveForwardDistInches(-24, 0.75);
        }
        driveTrain.driveForwardDistInches(-7,.25);
    }
}



package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous(name="FirstAutonCrater", group = "Playground")
@Disabled
public class FirstAutonCrater extends autonomous {
    public void runOpMode(){
        driveLift(1);
        driveForwardDistInches(200);
        turnDegrees(45, VertigoTankDrive.TurningDirection.LEFT);
        driveTrain.driveForwardDistInches(28);
        driveTrain.turnDegrees(45, VertigoTankDrive.TurningDirection.RIGHT);
        driveTrain.driveForwardDistInches(36);
        driveTrain.turnDegrees(45, VertigoTankDrive.TurningDirection.LEFT);
        driveTrain.driveForwardDistInches(144-20);

    }
}

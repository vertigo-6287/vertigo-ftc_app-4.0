package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer;
@Disabled
@Autonomous(name="Crater Autonomous", group = "Playground")
public class CraterAutonomous extends BaseOpModeFionaDrive {
    public void runOpMode(){
        //MusicPlayer.start(hardwareMap.appContext,MusicPlayer.hanukkahSong);
        super.runOpMode(OpModeType.AUTONOMOUS);//Do the initing that we defined in the BaseOpModeFionaDrive file
        dropFromLander();
        telemetry.addData("Position: ",position);
        telemetry.update();
        yeetThatGoldBoi(position);
        returnHome(position);
        craterToDepot();
        runToCrater();
        MusicPlayer.stop();

    }
}

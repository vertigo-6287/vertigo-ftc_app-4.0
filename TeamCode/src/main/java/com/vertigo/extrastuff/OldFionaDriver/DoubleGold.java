package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name="Double Gold", group = "Playground")
public class DoubleGold extends BaseOpModeFionaDrive {
    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);//Do the initing that we defined in the BaseOpModeFionaDrive file
        dropFromLander();
        GoldBlockPosition position = findBlock();
        yeetThatDoubleGoldBoi(position);
        //Now the gold boi has been yote, go deal with other side



    }
}

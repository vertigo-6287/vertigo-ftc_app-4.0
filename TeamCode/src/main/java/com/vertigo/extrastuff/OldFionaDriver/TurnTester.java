package com.vertigo.extrastuff.OldFionaDriver;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous(name = "Turning Tester", group = "Utilities")
@Disabled
public class TurnTester extends BaseOpModeFionaDrive {

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        waitForStart();

        telemetry.addData("status","turning const speed");
        telemetry.update();
        driveTrain.turnDegrees(90, VertigoTankDrive.TurningDirection.LEFT,.8);
        sleep(3000);

    }
}

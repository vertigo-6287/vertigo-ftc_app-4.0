
package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;


/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="Hawk-autonomous", group="Linear Opmode")
@Disabled
public class autonomous extends BaseOpModeFionaDrive {
   
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftDrive;
    private DcMotor rightDrive;
    private DcMotor leftDrive2;
    private DcMotor rightDrive2;
    private VertigoGyro gyro;
    Orientation angles;
    Acceleration gravity;
    BNO055IMU imu;
    Telemetry telemetry;




    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftDrive  = hardwareMap.get(DcMotor.class, "leftDrive");
        leftDrive2  = hardwareMap.get(DcMotor.class, "leftDrive2");
        rightDrive = hardwareMap.get(DcMotor.class, "rightDrive");
        rightDrive2 = hardwareMap.get(DcMotor.class, "rightDrive2");


        gyro = new VertigoGyro(hardwareMap.get(BNO055IMU.class, "imu"), telemetry);
        
        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery

        

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Setup a variable for each drive wheel to save power level for telemetry
            double leftPower;
            double rightPower;

            // Choose to drive using either Tank Mode, or POV Mode
            // Comment out the method that's not used.  The default below is POV.

            // POV Mode uses left stick to go forward, and right stick to turn.
            // - This uses basic math to combine motions and is easier to drive straight.
            //double drive = -gamepad1.left_stick_y;
            //double turn  =  gamepad1.right_stick_x;
            //leftPower    = Range.clip(drive + turn, -1.0, 1.0) ;
            //rightPower   = Range.clip(drive - turn, -1.0, 1.0) ;

            // Tank Mode uses one stick to control each wheel.
            // - This requires no math, but it is hard to drive forward slowly and keep straight.
             leftPower  = gamepad1.left_stick_y ;
             rightPower = -gamepad1.right_stick_y ;

            // Send calculated power to wheels
            driveTrain.driveLeft(leftPower);
            driveTrain.driveRight(rightPower);

          

            // Show the elapsed game time and wheel power.
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.addData("Motors", "left (%.2f), right (%.2f)", leftPower, rightPower);
            telemetry.addData("Heading", gyro.getHeading());
            telemetry.update();
        }
    }
    public void VertigoGyro (BNO055IMU vertImu, Telemetry telementry) {
        imu = vertImu;
        telemetry= telementry;
        init();
    }

    private void __init__() {

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        imu.initialize(parameters);

    }

    public float getheading() {

        angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        gravity  = imu.getGravity();
        return(AngleUnit.DEGREES.normalize(angles.firstAngle));
    }
    public void turnTo(int desired) {

        leftDrive  = hardwareMap.get(DcMotor.class, "leftDrive");
        leftDrive2  = hardwareMap.get(DcMotor.class, "leftDrive2");
        rightDrive = hardwareMap.get(DcMotor.class, "rightDrive");
        rightDrive2 = hardwareMap.get(DcMotor.class, "rightDrive2");

        while(getheading()>desired && opModeIsActive()){
            leftDrive.setPower(-1);
            leftDrive2.setPower(-1);
            rightDrive.setPower(1);
            rightDrive2.setPower(1);
        }
        while(getheading()<desired && opModeIsActive()) {
            leftDrive.setPower(-1);
            leftDrive2.setPower(-1);
            rightDrive.setPower(1);
            rightDrive2.setPower(1);
        }
    }
    public void driveForwardDistInches(int inches){

        double doubleTicks = Math.round(inches* 29.7089); // 29.7089 ticks per inch

        int ticks = (int)doubleTicks;

        leftDrive.setTargetPosition(ticks-leftDrive.getCurrentPosition());
        leftDrive2.setTargetPosition(ticks-leftDrive2.getCurrentPosition());
        rightDrive.setTargetPosition(ticks-rightDrive.getCurrentPosition());
        rightDrive2.setTargetPosition(ticks-rightDrive2.getCurrentPosition());

        leftDrive2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightDrive2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        leftDrive.setPower(1);
        leftDrive2.setPower(1);
        rightDrive.setPower(1);
        rightDrive2.setPower(1);

        while (leftDrive.isBusy() || leftDrive2.isBusy() || rightDrive.isBusy() || rightDrive2.isBusy() && opModeIsActive()) {
            // waits until the motors are done running
            telemetry.addData("Encoders running to:", ticks);
        }

        leftDrive.setPower(0);
        leftDrive2.setPower(0);
        rightDrive.setPower(0);
        rightDrive2.setPower(0);

        leftDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftDrive2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightDrive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightDrive2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

    }
    public void driveLeft(double power){
        leftDrive.setPower(power);
        leftDrive2.setPower(power);

    }
    public void driveRight(double power){
        rightDrive.setPower(power);
        rightDrive2.setPower(power);
    }
    public void turnDegrees(int degreesToTurn, VertigoTankDrive.TurningDirection direction){
        //this method turns a certain number of degrees
        int targetAngle = ((int) gyro.getHeading())+degreesToTurn;
        if (direction== VertigoTankDrive.TurningDirection.RIGHT) {
            while (gyro.getHeading()!=targetAngle && opModeIsActive()) {
                driveLeft(1);
                driveRight(-1);

            }
            driveLeft(0);
            driveRight(0);
        }
        else if (direction== VertigoTankDrive.TurningDirection.LEFT){
            while (gyro.getHeading()!=targetAngle && opModeIsActive()) {
                driveLeft(-1);
                driveRight(1);
            }
            driveLeft(0);
            driveRight(0);
        }

    }
    public void turnToAngle(int angle) {
        //This method turns until the gyro equals angle

        if (gyro.getHeading() < angle) {
            while (gyro.getHeading() != angle && opModeIsActive()) {
                driveLeft(1);
                driveRight(-1);

            }
            driveLeft(0);
            driveRight(0);
        } else if (gyro.getHeading() > angle) {
            while (gyro.getHeading() != angle && opModeIsActive()) {
                driveLeft(-1);
                driveRight(1);
            }
            driveLeft(0);
            driveRight(0);
        }
    }
}

package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name="Last Option Crater", group = "Playground")

public class LastOptionCrater extends BaseOpModeFionaDrive {
    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);//Do the initing that we defined in the BaseOpModeFionaDrive file
        dropFromLander();
        GoldBlockPosition position = findBlock();
        telemetry.addData("Position: ",position);
        telemetry.update();
        yeetThyGoldBoi(position);
        sleep(1000);
    }
}

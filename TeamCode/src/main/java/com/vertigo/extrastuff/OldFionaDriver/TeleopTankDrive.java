package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.vertigo.extrastuff.CenaPlayer;

import org.firstinspires.ftc.teamcode.NonOpModes.ControllerInput;

@Disabled
@TeleOp(name = "This is the TeleOP")
public class TeleopTankDrive extends BaseOpModeFionaDrive {

    public void runOpMode(){
        super.runOpMode(OpModeType.TELEOP);

        ControllerInput controllerInput = new ControllerInput(gamepad1,telemetry);
        int pwd = controllerInput.getInt("Please enter your password");

        if (pwd==CenaPlayer.password){
            telemetry.addData("Login:","Password Correct!");
            telemetry.update();
            sleep(2000);
        }else {
            telemetry.addData("Login:","Incorrect Password!!!");
            telemetry.update();
            sleep(2000);
            stop();
        }
        sleep(5000);
        while (opModeIsActive()) {
            telemetry.addData("gamepadleft", gamepad1.left_stick_y);
            telemetry.addData("gamepadright",gamepad1.right_stick_y);
            telemetry.addData("gamepada",gamepad1.a);
            telemetry.addData("gamebadb",gamepad1.b);
            telemetry.addData("redvalue", hardwareMap.get(ColorSensor.class,"liftColorSensor").red());
            telemetry.addData("bluevalue", hardwareMap.get(ColorSensor.class,"liftColorSensor").blue());
            telemetry.addData("greenvalue", hardwareMap.get(ColorSensor.class,"liftColorSensor").green());
            telemetry.addData("time",getRuntime());
            telemetry.addData("gyro",driveTrain.gyro.getHeading());
            //driveTrain.lf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            driveTrain.lb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            //driveTrain.rf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            driveTrain.rb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //telemetry.addData("lfticks",driveTrain.lf.getCurrentPosition());
            telemetry.addData("rbticks",driveTrain.rb.getCurrentPosition());
            telemetry.update();
            driveTrain.driveLeft(-gamepad1.left_stick_y);//both sticks are reversed
            driveTrain.driveRight(-gamepad1.right_stick_y);
            if (gamepad1.a){
                driveLift(1);
            } else if(gamepad1.b) {
                driveLift(-1);
            }else {
                driveLift(0);
            }
            if (gamepad1.left_bumper){
                //dropMarker();
            }
            if (gamepad1.right_bumper){
                getMarker().setDirection(Servo.Direction.REVERSE);
                getMarker().setPosition(0);
                telemetry.addData("servo",getMarker().getDirection());
                telemetry.addData("pos",getMarker().getPosition());
                telemetry.update();
                sleep(2000);
            }
            if (gamepad1.y){
                telemetry.addData("servo",getMarker().getDirection());
                telemetry.addData("pos",getMarker().getPosition());
                telemetry.update();
                sleep(2000);
            }
            if(gamepad2.dpad_up){
                smokingTheseMeats();
            }
            if (gamepad2.dpad_down){
                stopMakingNoise();
            }
            }
            stopMakingNoise();



        }

    }


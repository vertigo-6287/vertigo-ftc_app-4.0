package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous(name = "encoder ticks test",group = "Utilities")
@Disabled
public class FindEncoderTicks extends BaseOpModeFionaDrive {
    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        driveTrain.driveForwardDistInches(50);
        sleep(2000);
        stopMakingNoise();
    }

}

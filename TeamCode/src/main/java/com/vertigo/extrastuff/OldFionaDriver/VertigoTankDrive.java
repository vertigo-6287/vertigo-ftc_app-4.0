package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class VertigoTankDrive {
    DcMotor lf,lb,rf,rb;
    Telemetry telemetry;
    VertigoGyro gyro;
    BaseOpModeFionaDrive.OpModeType opModeType;
    enum TurningDirection{
        LEFT,RIGHT;
    }
    /**
     *
     * Old constructor remains here because I don't want to fix the references to it
     */
    public VertigoTankDrive(DcMotor LFRONT, DcMotor LBACK, DcMotor RFRONT, DcMotor RBACK, Telemetry telemetry){
        lf=LFRONT;
        lb=LBACK;
        rf=RFRONT;
        rb=RBACK;
        this.telemetry=telemetry;





        this.init();
    }
    public VertigoTankDrive(DcMotor LFRONT, DcMotor LBACK, DcMotor RFRONT, DcMotor RBACK, BNO055IMU vertImu, Telemetry telemetry){
        lf=LFRONT;
        lb=LBACK;
        rf=RFRONT;
        rb=RBACK;
        this.telemetry=telemetry;
        gyro=new VertigoGyro(vertImu,telemetry);


        this.init();
    }
    public VertigoTankDrive(DcMotor LBACK, DcMotor RBACK, BNO055IMU vertImu, Telemetry telemetry){

        lb=LBACK;

        rb=RBACK;
        this.telemetry=telemetry;
        gyro=new VertigoGyro(vertImu,telemetry);


        this.init();
    }
    /*
    Op mode for use in baseopmode extended opmodes
     */
    public VertigoTankDrive(DcMotor LFRONT, DcMotor LBACK, DcMotor RFRONT, DcMotor RBACK, BNO055IMU vertImu, Telemetry telemetry, BaseOpModeFionaDrive.OpModeType type){
        lf=LFRONT;
        lb=LBACK;
        rf=RFRONT;
        rb=RBACK;
        this.telemetry=telemetry;
        gyro=new VertigoGyro(vertImu,telemetry);
        opModeType=type;

        this.init();
    }

    public void driveLeft(double power){
        //lf.setPower(power);
        lb.setPower(power);

    }
    public void driveRight(double power){
        //rf.setPower(power);
        rb.setPower(power);
    }
    public void init(){
        //rf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        //lf.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lb.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        rb.setDirection(DcMotor.Direction.FORWARD);
        //rf.setDirection(DcMotor.Direction.FORWARD);
        lb.setDirection(DcMotor.Direction.REVERSE);
        //lf.setDirection(DcMotor.Direction.REVERSE);

        //rf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //lf.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        lb.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        //lf.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lb.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        //rf.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rb.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);


    }
    public void driveForwardDistInches(double inches, double power){

        double doubleTicks = Math.round(inches* 48.87); // 19.1082 ticks per inch

        int ticks = (int)doubleTicks;

        //lf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        lb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //rf.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rb.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        //lf.setTargetPosition(ticks+lf.getCurrentPosition());
        lb.setTargetPosition(lb.getCurrentPosition()+ticks);
        //rf.setTargetPosition(ticks+rf.getCurrentPosition());
        rb.setTargetPosition(ticks+rb.getCurrentPosition());
        telemetry.addData("targetticks",ticks+lb.getCurrentPosition());

        lb.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        //lf.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rb.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        //rf.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        //lf.setPower(1);
        lb.setPower(power);
        rb.setPower(power);
        //rf.setPower(1);

        while (lb.isBusy() && rb.isBusy()) {
            // waits until the motors are done running
            telemetry.addData("current ticks,busy", lb.getCurrentPosition());
            telemetry.update();
        }

        //lf.setPower(0);
        lb.setPower(0);
        //rf.setPower(0);
        rb.setPower(0);

        //lf.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        lb.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        //rf.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rb.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

    }
    //Same method, but full power if not specified
    public void driveForwardDistInches(int inches){
        driveForwardDistInches(inches,1);
    }


    public void turnDegrees(double degreesToTurn, TurningDirection direction){
        //this method turns a certain number of degrees
        double targetAngle = (((double) gyro.getHeading())+degreesToTurn)%360;
        if (direction==TurningDirection.RIGHT) {
            while (Math.abs(gyro.getHeading()-targetAngle) >3) {
                telemetry.addData("difference",Math.abs(gyro.getHeading()-targetAngle));
                telemetry.addData("heading",gyro.getHeading());
                telemetry.addData("target",targetAngle);
                telemetry.update();
                driveLeft(.75);
                driveRight(-.75);

            }
            driveLeft(0);
            driveRight(0);
        }
        else if (direction==TurningDirection.LEFT){
            while (Math.abs(gyro.getHeading()-targetAngle) >3) {
                driveLeft(-0.75);
                driveRight(0.75);
            }
            driveLeft(0);
            driveRight(0);
        }

    }
    public void turnDegrees(int degreesToTurn, TurningDirection direction,double power){
        //this method turns a certain number of degrees
        int targetAngle = (((int) gyro.getHeading())+degreesToTurn)%360;
        double turningDegreeThreashold=1;
        if (direction==TurningDirection.RIGHT) {
            while (Math.abs(gyro.getHeading()-targetAngle) >turningDegreeThreashold) {
                telemetry.addData("difference",Math.abs(gyro.getHeading()-targetAngle));
                telemetry.addData("heading",gyro.getHeading());
                telemetry.addData("target",targetAngle);
                telemetry.update();
                driveLeft(power);
                driveRight(-power);

            }
            driveLeft(0);
            driveRight(0);
        }
        else if (direction==TurningDirection.LEFT){
            while (Math.abs(gyro.getHeading()-targetAngle) >turningDegreeThreashold) {
                driveLeft(-power);
                driveRight(power);
            }
            driveLeft(0);
            driveRight(0);
        }

    }

    public void turnDegreesChangingPower(int degreesInput, TurningDirection direction, double power){
        int smallSection = (int)(0.2*degreesInput);
        int largeSection = (int)(0.6*degreesInput);
        double slowPower=power*.5;

        turnDegrees(smallSection,direction,slowPower);
        turnDegrees(largeSection,direction,power);
        turnDegrees(smallSection,direction,slowPower);

    }

    public void turnToAngle(int rawangle){
        int angle = rawangle % 360;
        //This method turns until the gyro equals angle

        if (gyro.getHeading()<angle) {
            while (Math.abs(gyro.getHeading()-angle) >3){
                driveLeft(1);
                driveRight(-1);

            }
            driveLeft(0);
            driveRight(0);
        }
        else if (gyro.getHeading()>angle){
            while (Math.abs(gyro.getHeading()-angle) >3) {
                driveLeft(-1);
                driveRight(1);
            }
            driveLeft(0);
            driveRight(0);
        }

        }


    public VertigoGyro getGyro() {
        return gyro;
    }
}

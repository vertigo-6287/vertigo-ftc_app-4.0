package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.hardware.bosch.BNO055IMU;

@TeleOp(name="teleop", group="Linear Opmode")
@Disabled
public abstract class teleop extends BaseOpModeFionaDrive {

    private VertigoGyro gyro;
    private GoldAlignDetector detector;
    private DcMotor leftDrive;
    private DcMotor rightDrive;
    private DcMotor leftDrive2;
    private DcMotor rightDrive2;
    private DcMotor up1;
    private DcMotor up2;

    public void runOpMode() {

        leftDrive  = hardwareMap.get(DcMotor.class, "leftDrive");
        leftDrive2  = hardwareMap.get(DcMotor.class, "leftDrive2");
        rightDrive = hardwareMap.get(DcMotor.class, "rightDrive");
        rightDrive2 = hardwareMap.get(DcMotor.class, "rightDrive2");
        up1 = hardwareMap.get(DcMotor.class, "lift");
        up2 = hardwareMap.get(DcMotor.class, "lift2");

        driveTrain = new VertigoTankDrive(leftDrive,leftDrive2,rightDrive,rightDrive2,hardwareMap.get(BNO055IMU.class, "imu"),telemetry);

        waitForStart();


        while(opModeIsActive()){
            leftDrive.setPower(gamepad1.left_stick_y);
            leftDrive2.setPower(gamepad1.left_stick_y);
            rightDrive.setPower(gamepad1.right_stick_y);
            rightDrive2.setPower(gamepad1.right_stick_y);

            if(gamepad1.a){
                up1.setPower(gamepad1.right_stick_y);
                up2.setPower(gamepad1.right_stick_y);
            }
        }
    }
}

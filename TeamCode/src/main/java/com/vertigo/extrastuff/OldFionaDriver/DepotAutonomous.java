package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name="Depot Autonomous", group = "Playground")
public class DepotAutonomous extends BaseOpModeFionaDrive {
    public void runOpMode() {
        super.runOpMode(OpModeType.AUTONOMOUS);//Do the initing that we defined in the BaseOpModeFionaDrive file
        dropFromLander();
        GoldBlockPosition position = findBlock();
        driveTrain.driveForwardDistInches(-3,.5);
        yeetThisGoldBoi(position);
        yeetTheMarker(position);
        /*
        if (position==GoldBlockPosition.LEFT){
            driveTrain.turnDegrees(285, VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(-43);
            dropMarker();
            sleep(500);
            driveTrain.driveForwardDistInches(3);
            driveTrain.turnDegrees(350,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(76);
        }
        if (position==GoldBlockPosition.RIGHT){
            driveTrain.turnDegrees(65,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(-45);
            dropMarker();
            sleep(500);
            driveTrain.turnDegrees(10,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(85);
        }
        if (position==GoldBlockPosition.CENTER){
            dropMarker();
            sleep(500);
            driveTrain.driveForwardDistInches(5);
            driveTrain.turnDegrees(70,VertigoTankDrive.TurningDirection.LEFT);
            driveTrain.driveForwardDistInches(10);
            driveTrain.turnDegrees(345,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(5,.5);
            driveTrain.turnDegrees(355,VertigoTankDrive.TurningDirection.RIGHT);
            driveTrain.driveForwardDistInches(65);
        }

*/
    }
}



package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name = "camera tester ext")
public class ExtCameraTester extends BaseOpModeFionaDrive {

    public void runOpMode(){
        super.runOpMode(OpModeType.TELEOP);
        sleep(1000);
        telemetry.update();
        sleep(4000);
        stopVuforiaLookingForBlock();

    }
}

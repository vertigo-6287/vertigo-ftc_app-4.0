package com.vertigo.extrastuff.OldFionaDriver;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous(name="ScratchPad")
@Disabled
public class ScratchPad extends BaseOpModeFionaDrive {
    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        telemetry.addData("Status", "in opmode");
        telemetry.update();
        sleep(2000);
        telemetry.addData("now","drive 1 inch");
        driveTrain.driveForwardDistInches(1);
        telemetry.update();
        sleep(2000);
        telemetry.addData("currentangle", driveTrain.gyro.getHeading());
        telemetry.update();
    }
}

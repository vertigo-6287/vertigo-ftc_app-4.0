package com.vertigo.extrastuff;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
@Disabled
@TeleOp(name = "Servo Tester")
public class ServoTester extends LinearOpMode {
    public void runOpMode(){
        waitForStart();
        while (opModeIsActive()){

            hardwareMap.servo.get("marker").setPosition(gamepad1.left_stick_y);
            telemetry.addData("pos",gamepad1.left_stick_y);
            telemetry.update();

        }
    }

}

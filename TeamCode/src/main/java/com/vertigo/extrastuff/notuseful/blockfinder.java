package com.vertigo.extrastuff.notuseful;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import com.qualcomm.hardware.bosch.BNO055IMU;
@TeleOp(name="blockfinder", group="Linear Opmode")
@Disabled


public class blockfinder extends LinearOpMode{


    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftDrive;
    private DcMotor rightDrive;
    private DcMotor leftDrive2;
    private DcMotor rightDrive2;
    Orientation angles;
    Acceleration gravity;
    BNO055IMU imu;
    Telemetry telemetry;


    public void setPower(DcMotor motor1, DcMotor motor2, double power){
        motor1.setPower(power);
        motor2.setPower(power);
    }

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftDrive = hardwareMap.get(DcMotor.class, "leftDrive");
        leftDrive2 = hardwareMap.get(DcMotor.class, "leftDrive2");
        rightDrive = hardwareMap.get(DcMotor.class, "rightDrive");
        rightDrive2 = hardwareMap.get(DcMotor.class, "rightDrive2");


        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
        leftDrive.setDirection(DcMotor.Direction.FORWARD);
        rightDrive.setDirection(DcMotor.Direction.FORWARD);


        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Setup a variable for each drive wheel to save power level for telemetry
            double leftPower;
            double rightPower;

            // Choose to drive using either Tank Mode, or POV Mode
            // Comment out the method that's not used.  The default below is POV.

            // POV Mode uses left stick to go forward, and right stick to turn.
            // - This uses basic math to combine motions and is easier to drive straight.
            //double drive = -gamepad1.left_stick_y;
            //double turn  =  gamepad1.right_stick_x;
            //leftPower    = Range.clip(drive + turn, -1.0, 1.0) ;
            //rightPower   = Range.clip(drive - turn, -1.0, 1.0) ;

            // Tank Mode uses one stick to control each wheel.
            // - This requires no math, but it is hard to drive forward slowly and keep straight.
            leftPower = gamepad1.left_stick_y;
            rightPower = -gamepad1.right_stick_y;

            // Send calculated power to wheels
            setPower(leftDrive, leftDrive2, leftPower);
            setPower(rightDrive, rightDrive2, rightPower);


            leftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            leftDrive2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            rightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            rightDrive2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);


            // Show the elapsed game time and wheel power.
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.addData("Motors", "left (%.2f), right (%.2f)", leftPower, rightPower);
            telemetry.update();
        }
    }

    public float getheading() {
        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        gravity = imu.getGravity();
        return (AngleUnit.DEGREES.normalize(angles.firstAngle));
    }
    public void go_to_and_back_ticks(int ticks) {
        //resets encoders
        leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftDrive2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDrive2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //goes forward
        leftDrive.setTargetPosition(ticks);
        leftDrive2.setTargetPosition(ticks);
        rightDrive.setTargetPosition(ticks);
        rightDrive2.setTargetPosition(ticks);
        //sets to go too
        leftDrive2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightDrive2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        //goes to
        leftDrive.setPower(1);
        leftDrive2.setPower(1);
        rightDrive2.setPower(1);
        rightDrive.setPower(1);

        int leftTicks = leftDrive.getCurrentPosition(); //gets the encoder ticks
        int leftTicks2 = leftDrive.getCurrentPosition();
        int rightTicks = rightDrive.getCurrentPosition();
        int rightTicks2 = rightDrive2.getCurrentPosition();
        //reverses moters
        leftDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        leftDrive2.setDirection(DcMotorSimple.Direction.REVERSE);
        rightDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        rightDrive2.setDirection(DcMotorSimple.Direction.REVERSE);
        //moves back
        leftDrive.setPower(1);
        leftDrive2.setPower(1);
        rightDrive2.setPower(1);
        rightDrive.setPower(1);
        while (leftDrive2.isBusy()) {
        }
    }
    public void forwardBack(float degrees,int ticks){
        while (getheading() < degrees) {
            leftDrive.setPower(1);
            leftDrive2.setPower(1);
            rightDrive2.setPower(-1);
            rightDrive.setPower(-1);
        }
        while (getheading() > degrees) {
            leftDrive.setPower(-1);
            leftDrive2.setPower(-1);
            rightDrive2.setPower(1);
            rightDrive.setPower(1);
        }
        go_to_and_back_ticks(ticks);


        while (getheading() > degrees) {
            leftDrive.setPower(-1);
            leftDrive2.setPower(-1);
            rightDrive2.setPower(1);
            rightDrive.setPower(1);
        }

    }


}

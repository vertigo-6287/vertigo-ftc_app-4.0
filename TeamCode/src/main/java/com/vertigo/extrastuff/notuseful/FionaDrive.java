package com.vertigo.extrastuff.notuseful;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotorImpl;
@Disabled
public class FionaDrive extends DcMotorImpl {
    private DcMotor motor1,motor2;


    public FionaDrive(DcMotorController controller, int portNumber) {
        super(controller, portNumber);
    }

    public void setPower(double power){
        motor1.setPower(power);
        motor2.setPower(power);
    }
    public void setDirection(DcMotor.Direction direction){
        motor1.setDirection(direction);
        motor2.setDirection(direction);
    }

}

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;


@Disabled
@Autonomous (name="TestRunningStrafe", group="Playground")

public class TestRunningStrafe extends MecanumBaseOpMode {
    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        waitForStart();


        //strafeDistInches(10,0.5,Direction.RIGHT);
        strafe(4,-95);
        sleep(5000);
    }
}

package org.firstinspires.ftc.teamcode.Local;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;

@Autonomous(name = "CraterAutonomous Simple",group = "Simple")
public class MechCraterAutonomous extends MecanumBaseOpMode {

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        telemetry.update();
        waitForStart();



        GoldBlockPosition goldBlockPosition= findBlock();

        dropFromLander();

        telemetry.addData("RUN position",goldBlockPosition);
        telemetry.update();

        move(5,Direction.BACKWARD,0.8,false,1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(800);
                raiseToLander();
            }
        }).start();

        knockGoldBlock(goldBlockPosition);
    }
}

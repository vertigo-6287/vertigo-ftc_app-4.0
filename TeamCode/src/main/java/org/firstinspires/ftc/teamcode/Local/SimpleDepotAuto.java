package org.firstinspires.ftc.teamcode.Local;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;

@Autonomous(name = "Simple Depot", group = "Simple")
public class SimpleDepotAuto extends MecanumBaseOpMode {

    public void runOpMode() {
        super.runOpMode(OpModeType.AUTONOMOUS);
        GoldBlockPosition goldBlockPosition;


        waitForStart();
        dropFromLander();

        goldBlockPosition = findBlock();
        telemetry.addData("RUN position", goldBlockPosition);
        telemetry.update();



        move(10, Direction.BACKWARD, 1, false, 5000);
        knockGoldBlock(goldBlockPosition);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(800);
                liftToHangingPosition();
            }
        }).start();

        //Center works!
        if (goldBlockPosition == GoldBlockPosition.CENTER) {
            telemetry.addData(">", "Running center depot code");
            telemetry.update();
            move(27, Direction.BACKWARD, 1, false, 7000);
            dropMarker();
            move(9, Direction.FORWARD, 1, false, 3000);
            turn(124, Direction.LEFT, 1, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(11, Diagonal.BR, 1, true);
            //move(1, Direction.RIGHT, 0.8, false, 2000);
            //Now do 5 degree
            fiveDegreeStrafe(2.3);

            //moveSpeedMin = 1;
            //move(47, Direction.BACKWARD, 1, false, 10000);
            /*
            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3500);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());
            */
            sleep(3000);
        }

        //This here right side works!
        if (goldBlockPosition == GoldBlockPosition.RIGHT) {
            move45(33, Diagonal.BL, 0.8);
            dropMarker();
            move(10, Direction.FORWARD, 1, false, 3000);
            turn(128, Direction.LEFT, 0.8, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(10, Diagonal.BR, 1, true);
            fiveDegreeStrafeSlow(2.9);
            /*
            move(1.5, Direction.RIGHT, 1, false, 2000);

            moveSpeedMin = 1;

            move(42, Direction.BACKWARD, 1, false, 10000);


            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3000);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());
            */
            sleep(3000);

        }

        if (goldBlockPosition == GoldBlockPosition.LEFT) {
            move45(33, Diagonal.BR, 0.8);
            dropMarker();
            move(10, Direction.FORWARD, 1, false, 3000);
            turn(124, Direction.LEFT, 0.8, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(18, Diagonal.BR, 0.8, true);
            fiveDegreeStrafeSlow(2.9);
            /*
            move(1, Direction.RIGHT, 0.8, false, 2000);

            moveSpeedMin = 1;
            move(38, Direction.BACKWARD, 1, false, 10000);

            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3000);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());
            */

            sleep(3000);

        }
    }

}



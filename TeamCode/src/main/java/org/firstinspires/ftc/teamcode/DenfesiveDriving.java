package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Disabled
@Autonomous(name = "Crater Denfesive",group = "Simple")
public class DenfesiveDriving extends MecanumBaseOpMode {

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        telemetry.update();
        waitForStart();




        dropFromLander();

        GoldBlockPosition goldBlockPosition= findBlock();
        telemetry.addData("RUN position",goldBlockPosition);
        telemetry.update();

        move(5,Direction.BACKWARD,0.8,false,1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(800);
                raiseToLander();
            }
        }).start();

        knockGoldBlock(goldBlockPosition);
        if (position == GoldBlockPosition.LEFT) {
            move45(32, Diagonal.FR, 0.6, false);
        } else if (position == GoldBlockPosition.RIGHT) {
            move45(32, Diagonal.FL, 0.6, false);
        } else if (position == GoldBlockPosition.CENTER) {
            move(26, Direction.FORWARD, 0.45, false, 3000);
        }
        move(5,Direction.RIGHT,0.8,false,1000);
        turn(45,Direction.RIGHT,0.45);
        move(35,Direction.FORWARD,0.8,false,1000);
        turn(90,Direction.RIGHT,0.45);

        extendArm(20, 1);
    }
}

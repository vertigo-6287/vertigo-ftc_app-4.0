package org.firstinspires.ftc.teamcode;


import com.qualcomm.hardware.rev.RevBlinkinLedDriver;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name = "Judging")
public class Judging extends MecanumBaseOpMode{

    @Override public void runOpMode(){
        super.runOpMode(OpModeType.TELEOP);
        waitForStart();
        resetStartTime();

        while (opModeIsActive()) {
            if(gamepad1.a){
                swoopStrafeRight(1.3,3,.3,3);
            }
            if(gamepad1.b){
                swoopStrafeLeft(1.3,3,.3,3);
            }
            if(gamepad1.y){
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.HEARTBEAT_RED);
                sleep(3000);
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.STROBE_RED);
                sleep(3000);
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.BLUE);
                sleep(2000);
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.GREEN);
            }
        }
    }
}

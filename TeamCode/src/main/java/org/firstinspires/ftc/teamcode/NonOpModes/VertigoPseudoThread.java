package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.hardware.HardwareMap;

import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class VertigoPseudoThread extends Thread {

    private HardwareMap hardwareMap;
    private volatile boolean isRunning;

    public VertigoPseudoThread(HardwareMap hardwareMap) {
        this.hardwareMap = hardwareMap;
    }

    public BlockingQueue<TaskData> queue = new LinkedBlockingQueue<TaskData>();

    public void run() {
        isRunning = true;
        TaskData task;
        while (isRunning) { //originally (task = queue.poll()) != null
            // process msg
            task = queue.poll();
            if (task != null) {
                if(new Date().getTime() - task.startTime > task.delay) {
                    //do stuff
                    task.task.runTask();
                }else{
                    queue.offer(task);
                }
            }
        }
    }
    public void shutDown(){
        isRunning=false;
    }

}

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer;
import org.firstinspires.ftc.teamcode.NonOpModes.TaskData;
import org.firstinspires.ftc.teamcode.NonOpModes.ThreadTaskInterface;

@Autonomous(name = "State Depot Auto",group = "Complex")
public class StateDepotAuto extends MecanumBaseOpMode {

    GoldBlockPosition goldBlockPosition;

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        waitForStart();
        ledThread.start();


        //Look at block, for now, this is constant
        goldBlockPosition=findBlock();
        telemetry.addData("RUN position",goldBlockPosition);
        dropFromLander();
        /**
         * Overview of autonomous algorithm
         *
         * 1. Find gold block
         * 2. Move forward to fully detach from lander
         * 2. In one motion, hit block off and land in depot
         * 3. Drop marker
         * 4. Turn and ride against wall into crater
         */


        //sleep(2000);


        if (goldBlockPosition==GoldBlockPosition.LEFT){
            move(10,Direction.BACKWARD,.7,false,2000);
            MusicPlayer.start(hardwareMap.appContext,MusicPlayer.whoopityscoop);
            swoopStrafeLeft(1.3,3,.3,3);
            move(8,Direction.BACKWARD,.8,false,1500);
            dropMarker();
            move(8,Direction.FORWARD,.8,false,1500);


            turn(120, Direction.LEFT, .8, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(12, Diagonal.BR, .8, false);
            fiveDegreeStrafe(2);

        }
        else if (goldBlockPosition==GoldBlockPosition.RIGHT){
            move(10,Direction.BACKWARD,.7,false,2000);
            MusicPlayer.start(hardwareMap.appContext,MusicPlayer.whoopityscoop);
            swoopStrafeRight(1.3,3,.3,3);
            move(10,Direction.BACKWARD,.8,false,1500);
            dropMarker();
            move(8,Direction.FORWARD,.8,false,1500);
            turn(120, Direction.LEFT, .8, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(16, Diagonal.BR, .8, false);
            fiveDegreeStrafe(2);
        }
        else if (goldBlockPosition == GoldBlockPosition.CENTER) {
            move(69,Direction.BACKWARD,.7,false,3000);
            dropMarker();
            move(8,Direction.FORWARD,.7,false,2000);
            turn(120, Direction.LEFT, .8, 0, 4000); //Check definition, pwr might be zero for debugging purposes
            move45(18, Diagonal.BR, .8, false);
            fiveDegreeStrafe(2.0);
        }


        sleep(3000);
    }
}

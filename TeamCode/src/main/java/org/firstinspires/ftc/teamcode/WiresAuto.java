package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.Position;

import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Blinker;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import java.util.List;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;


//An auto program to help out 16019
@Autonomous(name = "Wires Auto")
public class WiresAuto extends MecanumBaseOpMode {


    private Gyroscope imu;
    private CRServo ramie1;
    private CRServo ramie2;
    private CRServo harvester;
    private DcMotor right_motor;
    private DcMotor plecy;
    private DcMotor ramie;
    private DcMotor left_motor;
    private Blinker expansion_Hub_2;
    private int position;

    public void drive(double left_power, double right_power) {
        left_motor.setPower(left_power);
        right_motor.setPower(right_power);
    }

    public void stop_driving() {
        drive(0, 0);
    }


    public void turn_using_encoder(double left_power, double right_power, int left_distance, int right_distance) {
        left_motor.setMode(DcMotor.RunMode.RESET_ENCODERS);
        right_motor.setMode(DcMotor.RunMode.RESET_ENCODERS);

        left_motor.setTargetPosition(left_distance);
        right_motor.setTargetPosition(right_distance);

        left_motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        right_motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        drive(left_power, right_power);
        while (left_motor.isBusy() && right_motor.isBusy()) {

        }
        stop_driving();
        left_motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        right_motor.setMode(DcMotor.RunMode.RUN_TO_POSITION);

    }


    public void opuszczanie() throws InterruptedException {
        plecy.setMode(DcMotor.RunMode.RESET_ENCODERS);

        plecy.setTargetPosition(-13500);

        plecy.setMode(DcMotor.RunMode.RUN_TO_POSITION);


        plecy.setPower(1);
        while (plecy.isBusy()) {

        }
        stop_driving();
        plecy.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void opuszczanie2() throws InterruptedException {
        plecy.setMode(DcMotor.RunMode.RESET_ENCODERS);

        plecy.setTargetPosition(3000);

        plecy.setMode(DcMotor.RunMode.RUN_TO_POSITION);


        plecy.setPower(1);
        while (plecy.isBusy()) {

        }
        plecy.setPower(0);


        plecy.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }


    public void runOpMode() {
        left_motor = hardwareMap.dcMotor.get("left_motor");
        right_motor = hardwareMap.dcMotor.get("right_motor");
        plecy = hardwareMap.dcMotor.get("plecy");
        ramie = hardwareMap.dcMotor.get("ramie");
        left_motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        right_motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        harvester = hardwareMap.crservo.get("harvester");
        ramie1 = hardwareMap.crservo.get("ramie1");
        ramie2 = hardwareMap.crservo.get("ramie2");
        ramie1.setDirection(CRServo.Direction.REVERSE);
        harvester.setDirection(CRServo.Direction.REVERSE);
        left_motor.setDirection(DcMotor.Direction.REVERSE);
        right_motor.setDirection(DcMotor.Direction.REVERSE);
        ramie.setDirection(DcMotor.Direction.REVERSE);
        plecy.setDirection(DcMotor.Direction.REVERSE);

        goldBlockPosition = findBlock();
        telemetry.addData("position", goldBlockPosition);
        telemetry.update();
        waitForStart();
        try {
            opuszczanie();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        turn_using_encoder(1,1,120,120);

        //goldBlockPosition will now have left, right, or center

        while (opModeIsActive()) {
            if (goldBlockPosition == GoldBlockPosition.CENTER) {

                telemetry.addData("Position: ", "Center");
                turn_using_encoder(1, 1, 50, 50);
                turn_using_encoder(1, 1, 520, 520);

            } else if (goldBlockPosition == GoldBlockPosition.RIGHT) {

                telemetry.addData("Position: ", "Right");
                turn_using_encoder(1, 1, 90, 90);
                turn_using_encoder(1, 1, 200, -200);
                turn_using_encoder(1, 1, 500, 500);
                turn_using_encoder(1, 1, -450, -450);
                turn_using_encoder(1, 1, -200, 200);

            } else {

                telemetry.addData("Position: ", "Left");
                turn_using_encoder(1, 1, 90, 90);
                turn_using_encoder(1, 1, -150, 150);
                turn_using_encoder(1, 1, 500, 500);
                turn_using_encoder(1, 1, -450, -450);
                turn_using_encoder(1, 1, 150, -150);

            }

        }
    }
}


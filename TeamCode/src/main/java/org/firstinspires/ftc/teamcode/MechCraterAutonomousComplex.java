package org.firstinspires.ftc.teamcode;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;
@Disabled
@Autonomous(name = "CraterAutonomous Complex",group = "Complex")
public class MechCraterAutonomousComplex extends MecanumBaseOpMode {



    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        telemetry.update();
        waitForStart();

        dropFromLander();
        GoldBlockPosition goldBlockPosition= findBlock();
        telemetry.addData("RUN position",goldBlockPosition);
        telemetry.update();


        move(5,Direction.BACKWARD,0.8,false,1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(800);
                liftToHangingPosition();
            }
        }).start();

        //knockGoldBlock(goldBlockPosition);

       //retraceKnockGoldBlock(goldBlockPosition);
       restOfAuto();
        move(9, Direction.LEFT, .4, false, 2000);
        restOfAutoForConvinence();
       /*
        move(10, Direction.BACKWARD, 1, false, 1000);
        turn(90, Direction.LEFT, .7);
        whoopityScoopScoopDittyWhoop();

        multiThread.queue.offer(new TaskData(0, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                dropMarker();
            }
        }));
        move(4,Direction.LEFT,.1,false,2000);
        move(57, Direction.FORWARD, 1, false, 5000);
        sleep(2000);
        */


        multiThread.shutDown();
    }
}

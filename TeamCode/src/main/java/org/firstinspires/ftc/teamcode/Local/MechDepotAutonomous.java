package org.firstinspires.ftc.teamcode.Local;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;
import org.firstinspires.ftc.teamcode.NonOpModes.TaskData;
import org.firstinspires.ftc.teamcode.NonOpModes.ThreadTaskInterface;
@Disabled
@Autonomous(name="Depot Autonomous Complex",group = "Complex")
public class MechDepotAutonomous extends MecanumBaseOpMode {

    public void runOpMode(){
        super.runOpMode(OpModeType.AUTONOMOUS);
        GoldBlockPosition goldBlockPosition = findBlock();
        telemetry.addData("INIT position",goldBlockPosition);
        telemetry.update();
        waitForStart();



        telemetry.addData("RUN position",goldBlockPosition);
        telemetry.update();

        dropFromLander();
        goldBlockPosition = findBlock();

        moveSpeedMin=1;
        move(20,Direction.BACKWARD,1,false,5000);
        extendArm(29,-1);
        sleep(2000);
        multiThread.queue.offer(new TaskData(0, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                setArmPower(-1);
            }
        }));
        multiThread.queue.offer(new TaskData(750, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                setArmPower(0);


            }
        }));
        dropMarker();

        new Thread(new Runnable() {
            @Override
            public void run() {
                liftToHangingPosition();
            }
        }).start();

        suckInArm(28,1);
        move(16,Direction.FORWARD,1,false,2000);

        knockGoldBlock(goldBlockPosition);
        retraceKnockGoldBlockDepot(goldBlockPosition);
        restOfAutoDepot();
        move(12, Direction.LEFT, .4, false, 2000);
        multiThread.queue.offer(new TaskData(500, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                extendArm(12,1);

            }
        }));
        move(12,Direction.BACKWARD,.6,false,2000);
/*
        //Center works!
        if (goldBlockPosition==GoldBlockPosition.CENTER){
            telemetry.addData(">","Running center depot code");
            telemetry.update();
            move(27,Direction.BACKWARD,1,false,7000);
            dropMarker();
            move(9,Direction.FORWARD,1,false,3000);
            turn(124,Direction.LEFT,1,0,4000); //Check definition, pwr might be zero for debugging purposes
            move45(14,Diagonal.BR,1,true);
            move(1,Direction.RIGHT,0.8,false,2000);

            moveSpeedMin=1;
            move(47,Direction.BACKWARD,1,false,10000);
            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3500);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());
            sleep(3000);
        }

        //This here right side works!
        if (goldBlockPosition==GoldBlockPosition.RIGHT){
            move45(33,Diagonal.BL,0.8);
            dropMarker();
            move(10,Direction.FORWARD,1,false,3000);
            turn(128,Direction.LEFT,0.8,0,4000); //Check definition, pwr might be zero for debugging purposes
            move45(25,Diagonal.BR,1,true);
            move(1.5,Direction.RIGHT,1,false,2000);

            moveSpeedMin=1;

            move(42,Direction.BACKWARD,1,false,10000);




            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3000);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());

            sleep(3000);

        }

        if (goldBlockPosition==GoldBlockPosition.LEFT){
            move45(33,Diagonal.BR,0.8);
            dropMarker();
            move(10,Direction.FORWARD,1,false,3000);
            turn(128,Direction.LEFT,0.8,0,4000); //Check definition, pwr might be zero for debugging purposes
            move45(19,Diagonal.BR,0.8,true);
            move(1,Direction.RIGHT,0.8,false,2000);

            moveSpeedMin=1;
            move(38,Direction.BACKWARD,1,false,10000);

            armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            armExtender.setPower(1);
            sleep(3000);
            armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            armExtender.setTargetPosition(armExtender.getCurrentPosition());

            sleep(3000);

        }
*/

    }
}

package org.firstinspires.ftc.teamcode.NonOpModes;

public interface ThreadTaskInterface {
    public void runTask();
}

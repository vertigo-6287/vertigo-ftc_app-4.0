package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.NonOpModes.ControllerState;
import org.firstinspires.ftc.teamcode.NonOpModes.HumanSimulator;
import org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer;
import org.firstinspires.ftc.teamcode.NonOpModes.TaskData;
import org.firstinspires.ftc.teamcode.NonOpModes.ThreadTaskInterface;
@Disabled
@TeleOp(name = "Human Simulator Mecanum Teleop")
public class HumanSimulatorMecanumTeleOp extends MecanumBaseOpMode {

    double maxSpeed = 0.4;
    double initmaxspeed=0.4;
    double superspeed=1;
    boolean toggle = false;

    HumanSimulator notAHuman;
    ControllerState hsgamepad1,hsgamepad2;

    @Override
    public void runOpMode() {
        notAHuman=new HumanSimulator(gamepad1,gamepad2);

        super.runOpMode(OpModeType.TELEOP); //Initialize hardware; motors, etc. are accessible through interitance

        telemetry.addData("Ready to start program", "");
        telemetry.update();
        
        led.setPower(-1);

        waitForStart();


        //Playback


        while (opModeIsActive()&&notAHuman.replayIterator1.hasNext()) {
            
            //********** Gamepad 1 - Start + A **********

            double desiredHeading = (((Math.toDegrees(Math.atan2(hsgamepad1.left_stick_y, -hsgamepad1.left_stick_x)) + 360) % 360) + 180) % 360;

            //if (hsgamepad1.dpad_up) maxSpeed = Range.clip(maxSpeed += .005, 0, 1);
           // if (hsgamepad1.dpad_down) maxSpeed = Range.clip(maxSpeed -= .005, 0, 1);
            if (hsgamepad1.x) {
                led.setPower(-1);
            } else if (hsgamepad1.b){
                led.setPower(1);
            } else if (armLimitSwitch.getValue()==1) {
                led.setPower(.8);
            } else if (hsgamepad1.right_trigger >.2) {
                led.setPower(-1);
            } else
            {led.setPower(-.2);}


            if (hsgamepad1.right_trigger>0.2){
                maxSpeed = superspeed;
            } else{
                maxSpeed=initmaxspeed;
            }


            //Some BChay black magic to make the robot move
            if (hsgamepad1.left_stick_y == 0 && hsgamepad1.left_stick_x == 0 && hsgamepad1.right_stick_x == 0) {
                motorLeftFront.setPower(Math.abs(motorLeftFront.getPower()) <= .1 ? 0 : (motorLeftFront.getPower() > 0 ? motorLeftFront.getPower() - .1 : motorLeftFront.getPower() + .1));
                motorLeftBack.setPower(Math.abs(motorLeftBack.getPower()) <= .1 ? 0 : (motorLeftBack.getPower() > 0 ? motorLeftBack.getPower() - .1 : motorLeftBack.getPower() + .1));
                motorRightFront.setPower(Math.abs(motorRightFront.getPower()) <= .1 ? 0 : (motorRightFront.getPower() > 0 ? motorRightFront.getPower() - .1 : motorRightFront.getPower() + .1));
                motorRightBack.setPower(Math.abs(motorRightBack.getPower()) <= .1 ? 0 : (motorRightBack.getPower() > 0 ? motorRightBack.getPower() - .1 : motorRightBack.getPower() + .1));
            } else {

                if (!toggle) {
                    motorLeftFront.setPower(maxSpeed * (-hsgamepad1.left_stick_y - hsgamepad1.left_stick_x - hsgamepad1.right_stick_x));
                    motorLeftBack.setPower(maxSpeed * (-hsgamepad1.left_stick_y + hsgamepad1.left_stick_x - hsgamepad1.right_stick_x));
                    motorRightFront.setPower(maxSpeed * (-hsgamepad1.left_stick_y + hsgamepad1.left_stick_x + hsgamepad1.right_stick_x));
                    motorRightBack.setPower(maxSpeed * (-hsgamepad1.left_stick_y - hsgamepad1.left_stick_x + hsgamepad1.right_stick_x));
                }

                else{
                    motorLeftFront.setPower(maxSpeed * (hsgamepad1.left_stick_y + hsgamepad1.left_stick_x + -hsgamepad1.right_stick_x));
                    motorLeftBack.setPower(maxSpeed * (hsgamepad1.left_stick_y - hsgamepad1.left_stick_x + -hsgamepad1.right_stick_x));
                    motorRightFront.setPower(maxSpeed * (hsgamepad1.left_stick_y - hsgamepad1.left_stick_x - -hsgamepad1.right_stick_x));
                    motorRightBack.setPower(maxSpeed * (hsgamepad1.left_stick_y + hsgamepad1.left_stick_x - -hsgamepad1.right_stick_x));
                }
            }
            //Telemetries
            telemetry.addData("Maximum Speed", maxSpeed);
            telemetry.addData("Arm ticks",arm.getCurrentPosition());
            telemetry.addData("PseudoMultiThread Queue Items", multiThread.queue.size());
            telemetry.addData("MLB",motorLeftBack.getPower());
            telemetry.addData("limit",armLimitSwitch.getValue());
            telemetry.update();
            //Block for music player
            if (hsgamepad2.dpad_left) {
                MusicPlayer.start(hardwareMap.appContext);
            }
            if (hsgamepad2.dpad_right) {
                MusicPlayer.stop();
            }

            if (hsgamepad1.dpad_down){

                toggle = true;
            }else if(hsgamepad1.dpad_up) toggle = false;


            //Block for armExtender with dem encoder bois
            if (hsgamepad2.right_trigger > 0){
                armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                armExtender.setPower(1);
            }else if(hsgamepad2.left_trigger > 0) {
                armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                armExtender.setPower(-1);
            }else {
                //arm.setPower(0);
                armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                armExtender.setTargetPosition(armExtender.getCurrentPosition());
            }
            //Block for lift
            if (hsgamepad2.y){
                driveLift(1);
            } else if(hsgamepad2.a) {
                driveLift(-1);
            }else {
                driveLift(0);

            }
            //Block for arm with dem encoder bois
            if (hsgamepad1.left_trigger > 0){
                /*
                arm.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                arm.setPower(.85);
                */
                setArmPower(.85);
            }else if(hsgamepad1.left_bumper) {
                /*
                arm.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                arm.setPower(-.85);
                */
                setArmPower(-.85);
            }else {
                //arm.setPower(0);
                /*
                arm.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                arm.setTargetPosition(arm.getCurrentPosition());
                */
                setArmPower(0);
            }
            //Sweeper controls
            intake.setPower(hsgamepad2.left_stick_y);

            if (hsgamepad1.a){
                multiThread.queue.offer(new TaskData(500, new ThreadTaskInterface() {
                    @Override
                    public void runTask() {
                        MusicPlayer.start(hardwareMap.appContext);
                    }
                }));
                multiThread.queue.offer(new TaskData(7500, new ThreadTaskInterface() {
                    @Override
                    public void runTask() {
                        MusicPlayer.stop();
                    }
                }));
            }
            if (hsgamepad1.y){
                multiThread.queue.offer(new TaskData(2000, new ThreadTaskInterface() {
                    @Override
                    public void runTask() {
                        led.setPower(1);
                        sleep(5000);
                        led.setPower(-1);
                        sleep(1500);
                    }
                }));
                sleep(200);
            }
        }
        //Opmode is no longer active, run the endtime code
        multiThread.shutDown();

    }
}
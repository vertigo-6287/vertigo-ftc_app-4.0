package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.rev.RevBlinkinLedDriver;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.teamcode.NonOpModes.ControllerInput;
import org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer;
import org.firstinspires.ftc.teamcode.NonOpModes.TaskData;
import org.firstinspires.ftc.teamcode.NonOpModes.ThreadTaskInterface;
import org.firstinspires.ftc.teamcode.NonOpModes.VertigoTensorFlow;

import static java.lang.Thread.interrupted;
import static org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer.whoopityscoop;
import static org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer.youregood;

@TeleOp(name = "Mecanum Teleop")
public class MecanumTeleOp extends MecanumBaseOpMode {

    double maxSpeed = 0.4;
    double initmaxspeed = 0.6;
    double superspeed = 1;
    boolean toggle = true;
    double gameTime = 120;
    boolean startedHanging = false;

    @Override
    public void runOpMode() {
        super.runOpMode(OpModeType.TELEOP); //Initialize hardware; motors, etc. are accessible through interitance

        blinkin.setPattern(defaultLEDPattern);
        telemetry.addData("Ready to start program", "");
        telemetry.update();
        waitForStart();
        resetStartTime();
        //MusicPlayer.start(hardwareMap.appContext);

        while (opModeIsActive()) {
            //********** Gamepad 1 - Start + A **********

            double desiredHeading = (((Math.toDegrees(Math.atan2(gamepad1.left_stick_y, -gamepad1.left_stick_x)) + 360) % 360) + 180) % 360;

            //if (gamepad1.dpad_up) maxSpeed = Range.clip(maxSpeed += .005, 0, 1);
            // if (gamepad1.dpad_down) maxSpeed = Range.clip(maxSpeed -= .005, 0, 1);
            if (gamepad1.x) {
                led.setPower(-.2);
            } else if (gamepad1.b) {
                led.setPower(.2);
            } else if (gamepad2.b) {
                led.setPower(.3);
            } else if (gamepad1.right_trigger > .2) {
                led.setPower(-.5);
            } else {
                led.setPower(-.2);
            }

            if (gameTime - getRuntime() > 30) {
                if (gamepad1.left_stick_y < -0.15) {
                    blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.SHOT_BLUE);
                } else if (gamepad1.left_stick_y > 0.15) {
                    blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.SHOT_RED);
                } else {
                    blinkin.setPattern(defaultLEDPattern);
                }
            } else if (gameTime - getRuntime() < 0) {
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.GREEN);
            } else if (gameTime - getRuntime() < 30) {
                if (gamepad2.a && !startedHanging) {
                    startedHanging = true;
                    //Sound effect here
                    MusicPlayer.start(hardwareMap.appContext,youregood);
                } else if (!startedHanging) {
                    blinkin.setPattern((gameTime - getRuntime()) < 15 ? RevBlinkinLedDriver.BlinkinPattern.STROBE_RED : RevBlinkinLedDriver.BlinkinPattern.HEARTBEAT_RED);
                } else {
                    blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.BLUE);
                }
            }


            if (gamepad1.right_trigger > 0.2) {
                maxSpeed = superspeed;
            } else {
                maxSpeed = initmaxspeed;
            }

            if (gamepad1.y){
                safetyOverride=!safetyOverride;
                sleep(1000);
            }


            //Some BChay black magic to make the robot move
            if (gamepad1.left_stick_y == 0 && gamepad1.left_stick_x == 0 && gamepad1.right_stick_x == 0) {
                motorLeftFront.setPower(Math.abs(motorLeftFront.getPower()) <= .1 ? 0 : (motorLeftFront.getPower() > 0 ? motorLeftFront.getPower() - .1 : motorLeftFront.getPower() + .1));
                motorLeftBack.setPower(Math.abs(motorLeftBack.getPower()) <= .1 ? 0 : (motorLeftBack.getPower() > 0 ? motorLeftBack.getPower() - .1 : motorLeftBack.getPower() + .1));
                motorRightFront.setPower(Math.abs(motorRightFront.getPower()) <= .1 ? 0 : (motorRightFront.getPower() > 0 ? motorRightFront.getPower() - .1 : motorRightFront.getPower() + .1));
                motorRightBack.setPower(Math.abs(motorRightBack.getPower()) <= .1 ? 0 : (motorRightBack.getPower() > 0 ? motorRightBack.getPower() - .1 : motorRightBack.getPower() + .1));
            } else {

                if (!toggle) {
                    motorLeftFront.setPower(maxSpeed * (-gamepad1.left_stick_y - gamepad1.left_stick_x - gamepad1.right_stick_x));
                    motorLeftBack.setPower(maxSpeed * (-gamepad1.left_stick_y + gamepad1.left_stick_x - gamepad1.right_stick_x));
                    motorRightFront.setPower(maxSpeed * (-gamepad1.left_stick_y + gamepad1.left_stick_x + gamepad1.right_stick_x));
                    motorRightBack.setPower(maxSpeed * (-gamepad1.left_stick_y - gamepad1.left_stick_x + gamepad1.right_stick_x));
                } else {
                    motorLeftFront.setPower(maxSpeed * (gamepad1.left_stick_y + gamepad1.left_stick_x + -gamepad1.right_stick_x));
                    motorLeftBack.setPower(maxSpeed * (gamepad1.left_stick_y - gamepad1.left_stick_x + -gamepad1.right_stick_x));
                    motorRightFront.setPower(maxSpeed * (gamepad1.left_stick_y - gamepad1.left_stick_x - -gamepad1.right_stick_x));
                    motorRightBack.setPower(maxSpeed * (gamepad1.left_stick_y + gamepad1.left_stick_x - -gamepad1.right_stick_x));
                }
            }
            //Telemetries
            telemetry.addData("Maximum Speed", maxSpeed);
            telemetry.addData("PseudoMultiThread Queue Items", multiThread.queue.size());
            telemetry.addData("Safety Override", safetyOverride ? "~~~!~~ENGAGED~~!~~~" : "DISENGAGED");
            telemetry.addData("Intake Lock Position : ", intakeLock.getPosition());
            telemetry.addData("Game time remaining", gameTime - getRuntime());
            telemetry.addData("Potentiometer",armPotentiometer.getVoltage());
            telemetry.update();

            double intakeToggle;
            if (gamepad2.b) {
                intakeLock.setPosition(.8);
                intakeToggle = 0.05;
            } else {
                intakeLock.setPosition(.375);
                intakeToggle = 1;
            }

            if (gamepad2.x)

            if (intakeLock.getPosition() != .375 && !gamepad2.b) {
                intakeLock.setPosition(.375);
            }


            //Block for music player
            if (gamepad2.dpad_left) {
                MusicPlayer.start(hardwareMap.appContext, whoopityscoop);
            }
            if (gamepad2.dpad_right) {
                MusicPlayer.stop();
            }

            if (gamepad1.dpad_down) {

                toggle = true;
            } else if (gamepad1.dpad_up) toggle = false;


            //Block for armExtender with dem encoder bois
            if (gamepad2.right_trigger > 0) {
                armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                armExtender.setPower(1);
            } else if (gamepad2.left_trigger > 0) {
                armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                armExtender.setPower(-1);
            } else {
                //arm.setPower(0);
                armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                armExtender.setTargetPosition(armExtender.getCurrentPosition());
            }
            //Block for lift
            if (gamepad2.y) {
                driveLift(1);
            } else if (gamepad2.a) {
                driveLift(-1);
            } else {
                driveLift(0);

            }
            //Block for arm with dem encoder bois
            if (gamepad1.left_trigger > 0) {
                setArmPower(-.85);
            } else if (gamepad1.left_bumper) {
                setArmPower(.85);
                /*else{
                    multiThread.queue.offer(new TaskData(0, new ThreadTaskInterface() {
                            @Override
                            public void runTask() {
                            while(!armAtDump()){
                                setArmPower(0.85); //85
                                telemetry.addData("volt",armPotentiometer.getVoltage());
                                telemetry.addData("target",ARM_DUMP_POSITION);
                                telemetry.update();
                            }
                        }
                    }));
                }*/
            } else{
                setArmPower(0);
            }

            //Sweeper controls
            if (!gamepad2.x) {
                intake.setPower((intakeToggle) * (-gamepad2.left_stick_y));
            }else{
                intake.setPower(-.03);
            }
            if(gamepad2.right_bumper){
                intake.setPower(.03);
            }

            if (gamepad2.dpad_down) {
                dropMarker();

            }



        }
        //Opmode is no longer active, run the endtime code
        multiThread.shutDown();
        MusicPlayer.stop();

    }
}
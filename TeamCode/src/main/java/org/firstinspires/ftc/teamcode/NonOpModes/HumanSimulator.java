package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.hardware.Gamepad;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import static android.os.SystemClock.sleep;

public class HumanSimulator {
    Gamepad gamepad1,gamepad2;
    ArrayList<ControllerState> actionList1 = new ArrayList<ControllerState>();
    ArrayList<ControllerState> actionList2 = new ArrayList<ControllerState>();
    public Iterator<ControllerState> replayIterator1;
    public Iterator<ControllerState> replayIterator2;
    public void record(int time, int timeInterval){
        long initTime = new Date().getTime();
        while(new Date().getTime()-initTime<time){
            //do the recording
            actionList1.add(new ControllerState(gamepad1));
            actionList1.add(new ControllerState(gamepad2));
            sleep(timeInterval);
        }
    }
    public void prepareForPlayback(){
        replayIterator1= actionList1.iterator();
        replayIterator2= actionList2.iterator();
    }
    public ControllerState getNextAction1(){
        if (replayIterator1.hasNext()) {
            ControllerState c = replayIterator1.next();
            replayIterator1.remove();
            return c;
        }
        else return null;
    }
    public ControllerState getNextAction2(){
        if (replayIterator2.hasNext()) {
            ControllerState c = replayIterator2.next();
            replayIterator2.remove();
            return c;
        }
        else return null;
    }

    public HumanSimulator(Gamepad gamepad1, Gamepad gamepad2) {
        this.gamepad1 = gamepad1;
        this.gamepad2=gamepad2;
    }

    @Override
    public String toString() {
        return actionList1.toString();
    }
}

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

@Autonomous(name = "Double DADDY",group = "Complex")
public class doubleDaddy extends MecanumBaseOpMode {



    public void runOpMode() {
        super.runOpMode(OpModeType.AUTONOMOUS);

       // moveSpeedMin = .8;

        waitForStart();
        ledThread.start();

        GoldBlockPosition goldBlockPosition = findBlock();
        telemetry.addData("Position",goldBlockPosition);
        telemetry.update();
        dropFromLander();



        if (goldBlockPosition == GoldBlockPosition.LEFT){
            move(7,Direction.BACKWARD,.4,false,5000);
            move45(28, Diagonal.BL, 0.4);
            move(6,Direction.FORWARD,.3,false,6000);
            turn(135,Direction.LEFT,.3);
            move45(14,Diagonal.BR,.3,false);
            sleep(2000);
            fiveDegreeStrafe(1.3);
            //dropMarker();
            //fiveDegreeStrafeForward(2.3);


        }

        if (goldBlockPosition == GoldBlockPosition.RIGHT){
            move(7,Direction.BACKWARD,.5,false,4000);
            move45(28, Diagonal.BR, 0.5);
            move45(16, Diagonal.FR, 0.5);

            turn(90,Direction.LEFT,.4);
            move(53,Direction.BACKWARD,.7,false,8000);
            turn(45,Direction.LEFT,.5);
            move45(20,Diagonal.BR,.6,false);
            fiveDegreeStrafe(1.4);
            //dropMarker();
            //fiveDegreeStrafeForward(2.4);
        }

        if (goldBlockPosition == GoldBlockPosition.CENTER){
            move(29, Direction.BACKWARD, 0.6, false, 8000);
            move(15,Direction.FORWARD,.6,false,8000);
            turn(90,Direction.LEFT,.7);
            move(48,Direction.BACKWARD,.6,false,9000);
            turn(45,Direction.LEFT,.8);
            move45(10,Diagonal.BR,.6,false);
            fiveDegreeStrafe(1.1);
            move45(31,Diagonal.FR,0.5,false);
            waitForMotors();
            //dropMarker();
            // fiveDegreeStrafeForward(2.4);

        }
    }
}
package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;

@Autonomous(name = "Internal camera tester",group = "Utilities")
public class InternalCameraTester extends MecanumBaseOpMode {

    public void runOpMode(){
        int count =0;
        super.runOpMode(OpModeType.TELEOP);
        waitForStart();
        while (opModeIsActive()) {
            GoldBlockPosition goldBlockPosition = findBlock();
            telemetry.addData("Gold Block Position Detected:", goldBlockPosition);
            telemetry.addData("Count:",count);
            telemetry.update();
            sleep(2000);
            telemetry.addData("UPDATING:",count);
            telemetry.update();
            count++;
        }
    }
}

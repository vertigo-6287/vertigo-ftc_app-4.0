package org.firstinspires.ftc.teamcode.NonOpModes;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.R;

import java.io.IOException;

@Disabled
public class MusicPlayer {
    //The player handling the audio
    private static MediaPlayer mediaPlayer = null;

    public static int zuccSong = R.raw.zuccbotaudio;
    public static int hanukkahSong = R.raw.hanukkahohhanukkah;
    public static int whoopityscoop = R.raw.whoopityscoop;
    public static int youregood = R.raw.youregood;
    //Start the wubs
    public static void start(Context context) {
        if (mediaPlayer == null) mediaPlayer = MediaPlayer.create(context, zuccSong);
        mediaPlayer.seekTo(0);
        mediaPlayer.start();
    }
    public static void start(Context context, int song) {
        mediaPlayer = MediaPlayer.create(context, song);
        mediaPlayer.seekTo(0);
        mediaPlayer.start();
    }

    //Stop the wubs
    public static void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            try { mediaPlayer.prepare(); }
            catch (IOException e) {}
        }
    }


}
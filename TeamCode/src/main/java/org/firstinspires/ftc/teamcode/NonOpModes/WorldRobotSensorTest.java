package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.MecanumBaseOpMode;

@TeleOp(name = "World Robot Sensor Test",group = "Utilities")
public class WorldRobotSensorTest extends MecanumBaseOpMode {
    public void runOpMode(){
        super.runOpMode(OpModeType.TELEOP);
        waitForStart();
        while (opModeIsActive()){
            telemetry.addData("Runtime",getRuntime());
            telemetry.addData("Potentiometer Data",armPotentiometer.getVoltage());
            telemetry.addData("Poten max v",armPotentiometer.getMaxVoltage());
            telemetry.addData("poten con inf",armPotentiometer.getConnectionInfo());
            telemetry.addData("Floor Color Sensor Data",floorColorSensor.argb());
            telemetry.update();
            //Block for arm with dem encoder bois
            if (gamepad1.left_trigger > 0) {
                setArmPower(-.85);
            } else if (gamepad1.left_bumper) {
                setArmPower(.85);
            } else {
                setArmPower(0);
            }
        }
    }
}

package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.hardware.Gamepad;


public class ControllerState {

    public boolean y,x,a,b,dpad_up,dpad_down,dpad_left,dpad_right,left_bumper,right_bumper;
    public float left_trigger,right_trigger,left_stick_x,left_stick_y,right_stick_x,right_stick_y;
    //something about current runtime since start
    Gamepad gamepad;
     public ControllerState(Gamepad gamepad){
         y=gamepad.y;
         x=gamepad.x;
         a=gamepad.a;
         b=gamepad.b;
         dpad_up=gamepad.dpad_up;
         dpad_down=gamepad.dpad_down;
         dpad_left=gamepad.dpad_left;
         dpad_right=gamepad.dpad_right;
         left_bumper=gamepad.left_bumper;
         right_bumper=gamepad.right_bumper;
         left_trigger=gamepad.left_trigger;
         right_trigger=gamepad.right_trigger;
         left_stick_x=gamepad.left_stick_x;
         left_stick_y=gamepad.left_stick_y;
         right_stick_x=gamepad.right_stick_x;
         right_stick_y=gamepad.right_stick_y;


     }

    @Override
    public String toString() {
        return "ControllerState{" +
                "y=" + y +
                ", x=" + x +
                ", a=" + a +
                ", b=" + b +
                ", dpad_up=" + dpad_up +
                ", dpad_down=" + dpad_down +
                ", dpad_left=" + dpad_left +
                ", dpad_right=" + dpad_right +
                ", left_bumper=" + left_bumper +
                ", right_bumper=" + right_bumper +
                ", left_trigger=" + left_trigger +
                ", right_trigger=" + right_trigger +
                ", left_stick_x=" + left_stick_x +
                ", left_stick_y=" + left_stick_y +
                ", right_stick_x=" + right_stick_x +
                ", right_stick_y=" + right_stick_y +
                '}';
    }
}

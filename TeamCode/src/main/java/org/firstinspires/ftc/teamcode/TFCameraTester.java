package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.NonOpModes.MusicPlayer;
import org.firstinspires.ftc.teamcode.NonOpModes.VertigoTensorFlow;
@Disabled
@TeleOp(name = "TFCameraTester")
public class TFCameraTester extends LinearOpMode {

    public void runOpMode(){
        telemetry.addData(">","ReAdY tO gO");
        telemetry.update();

        waitForStart();


            sleep(300);
                //MusicPlayer.start(hardwareMap.appContext, MusicPlayer.zuccSong);
                telemetry.addData("Position",new VertigoTensorFlow(hardwareMap, telemetry).trackBlock(5000));
                telemetry.update();
                sleep(3000);


        MusicPlayer.stop();
    }
}

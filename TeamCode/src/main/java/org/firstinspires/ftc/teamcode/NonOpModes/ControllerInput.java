package org.firstinspires.ftc.teamcode.NonOpModes;

import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import static android.os.SystemClock.sleep;

public class ControllerInput {
    private Gamepad gamepad;
    Telemetry telemetry;
    String theInt;

    public ControllerInput(Gamepad gamepad, Telemetry telemetry) {
        this.gamepad = gamepad;
        this.telemetry = telemetry;
    }

    public int getSingleInt(String prompt){
        while (true){
            if (gamepad.a) return 1;
            if (gamepad.b) return 2;
            if (gamepad.y) return 3;
            if (gamepad.x) return 4;

            if (gamepad.dpad_down) return 5;
            if (gamepad.dpad_right) return 6;
            if (gamepad.dpad_up) return 7;
            if (gamepad.dpad_left) return 8;

            if (gamepad.left_bumper) return 9;
            if (gamepad.right_bumper) return 0;

            if (gamepad.left_trigger>0.2) return -2;
            if (gamepad.right_trigger>0.2) return -1;

            telemetry.addData("Numberpad",prompt);
            telemetry.addData("lt",gamepad.left_trigger);
            telemetry.addData("rt",gamepad.right_trigger);
            telemetry.addData("Currently entered value: ",theInt);
            telemetry.update();

        }
    }

    public int getInt(String prompt){
        theInt="";
        int next;

        while (true){
            telemetry.addData("Numberpad",prompt);
            telemetry.addData("Currently entered value: ",theInt);
            telemetry.update();
            next=getSingleInt(prompt);
            if (next==-1){
                break;
            }
            else if(next==-2){
                theInt="";
            }
            else{
                theInt+=next;
            }
            sleep(500);
            telemetry.addData("state", "aftersleep");
            telemetry.update();

        }
        return Integer.parseInt(theInt);
    }
    public int getInt(){
        return getInt("Please enter a number");
    }
}

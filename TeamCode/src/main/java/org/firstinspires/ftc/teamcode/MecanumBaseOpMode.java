package org.firstinspires.ftc.teamcode;

import com.disnodeteam.dogecv.CameraViewDisplay;
import com.disnodeteam.dogecv.DogeCV;
import com.disnodeteam.dogecv.detectors.roverrukus.GoldAlignDetector;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.hardware.rev.RevBlinkinLedDriver;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;
import com.vertigo.extrastuff.OldFionaDriver.VertigoGyro;

import static com.qualcomm.robotcore.hardware.DcMotor.RunMode.RUN_TO_POSITION;
import static com.qualcomm.robotcore.hardware.DcMotor.RunMode.RUN_USING_ENCODER;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;
import org.firstinspires.ftc.teamcode.NonOpModes.TaskData;
import org.firstinspires.ftc.teamcode.NonOpModes.ThreadTaskInterface;
import org.firstinspires.ftc.teamcode.NonOpModes.VertigoPseudoThread;
import org.opencv.core.Point;

public abstract class MecanumBaseOpMode extends LinearOpMode {

    DcMotor motorLeftFront;
    DcMotor motorLeftBack;
    DcMotor motorRightFront;
    DcMotor motorRightBack;
    DcMotor armExtender;
    DcMotor lift;
    DcMotor led; //-1 produces blue light, 1 produces red light

    RevBlinkinLedDriver blinkin;

    DcMotor arm;
    BNO055IMU imu;
    CRServo intake;
    Servo intakeLock;
    Servo marker;
    TouchSensor armLimitSwitch;
    public AnalogInput armPotentiometer;
    public ColorSensor floorColorSensor;

    public VertigoPseudoThread multiThread = new VertigoPseudoThread(hardwareMap);

    int gronk = 69;
    boolean chay = false;

    public GoldAlignDetector detector;
    public GoldBlockPosition position;
    public int ENCODER_THRESHOLD = 5;
    boolean safetyOverride;

    GoldBlockPosition goldBlockPosition;
    public volatile boolean manualControlLED=false;
    public Thread ledThread;

    public double moveSpeedMin = .1;
    double moveSpeedMax = .4;
    public double ticksRatioForward = 55.185; //Ticks / Inch
    public double ticksRatioStrafe = 85.68;
    public double ticksRatioDiagonal = 80;
    public double ticksRatioArm = 164;

    public static double ARM_MAX_POSITION = 0.345;
    public static double ARM_MIN_POSITION = 2.6;
    public static double ARM_DUMP_POSITION = 1
            ;

    public static RevBlinkinLedDriver.BlinkinPattern defaultLEDPattern = RevBlinkinLedDriver.BlinkinPattern.RAINBOW_RAINBOW_PALETTE;



    public enum Direction {
        FORWARD, BACKWARD, LEFT, RIGHT;
    }

    public enum OpModeType {
        AUTONOMOUS, TELEOP;
    }

    public enum Diagonal {
        FR, FL, BR, BL
    }

    public enum GoldBlockPosition {
        LEFT, RIGHT, CENTER;
    }



    //General Variables
    public double previousHeading = 0; //Saved heading from previous check, used to calculate integratedHeading
    public double integratedHeading = 0; //Saves heading in range (-inf, +inf) instead of (-179, 179)
    double turnSpeedMin = .25;
    public static final String VUFORIA_KEY = "AfWNQ2D/////AAABmfW4VE+p6kf9r9cRtloxh3uI/ouYzQ7AMbw6ORdx/9/uqt3G9++uJuv2zbmrwMhKgENFB0cTCwFy2VxtqYNKv3EyCh7Jlty5ru7DfBOF2XvF6k7pFIWJVWto+sxB2xZNJ4BOgakITKwefHslI/Qau/yw1osgbD0Dx5BLinN2+iwSp8y5wHxRte5eVB/upNXKFvHREyT2W6imbplz7/guzBcmMqNg9P7VIeCf51ijZqBS24ppmgbsU06RtsclBfGoR3kRiHE91HTM4rKyVvQ13m4zfAX2ncVWa5STBSVoGWVoBR/At+ngTV5aiWoFgP9nXvf9ThK5gts5lsmQhBMWx6U+vRqTiBVHUYFw9eMeUyCR";



    public void mapHardware() {

        telemetry.addData("Init status","Mapping Hardware");
        telemetry.update();

        motorLeftFront = hardwareMap.dcMotor.get("motorLeftFront");
        motorLeftBack = hardwareMap.dcMotor.get("motorLeftBack");
        motorRightFront = hardwareMap.dcMotor.get("motorRightFront");
        motorRightBack = hardwareMap.dcMotor.get("motorRightBack");
        armExtender = hardwareMap.dcMotor.get("armExtender");//Part of the arm that has the vertical slide
        lift = hardwareMap.dcMotor.get("lift");
        imu = hardwareMap.get(BNO055IMU.class, "imu 1");
        arm = hardwareMap.dcMotor.get("arm"); //Arm that swings/pivots up and down
        intake = hardwareMap.crservo.get("intake");//Cont. servo that drives the intake to pick up elements
        led = hardwareMap.dcMotor.get("led");
        armLimitSwitch = hardwareMap.touchSensor.get("armLimitSwitch");
        armPotentiometer = hardwareMap.analogInput.get("armPotentiometer");
        floorColorSensor = hardwareMap.colorSensor.get("floorColorSensor");
        marker = hardwareMap.servo.get("marker");
        intakeLock = hardwareMap.servo.get("intakeLock");
        blinkin = hardwareMap.get(RevBlinkinLedDriver.class,"blinkin");


        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        parameters.mode = BNO055IMU.SensorMode.IMU;

        imu.initialize(parameters);
        imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);


    }

    public void runOpMode(OpModeType type) {

        telemetry.addData("init status","runnnig opmode");
        telemetry.update();
        safetyOverride = false;
        multiThread.start();
        mapHardware();
        telemetry.addData("init stauts","Configuring motors");
        telemetry.update();
        motorLeftFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        motorLeftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorLeftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorRightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        motorRightBack.setDirection(DcMotorSimple.Direction.REVERSE);
        motorRightFront.setDirection(DcMotorSimple.Direction.REVERSE);
        motorLeftBack.setDirection(DcMotorSimple.Direction.FORWARD);
        motorLeftFront.setDirection(DcMotorSimple.Direction.FORWARD);

        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armExtender.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        telemetry.addData("init status","Setting LED and intake lock");
        telemetry.update();


        intakeLock.setPosition(.375);
        if (type == OpModeType.AUTONOMOUS) {

            ledThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    double startTime = getRuntime();
                    while (getRuntime()-startTime<30) {
                        led.setPower(-0.2);
                        sleep(2000);
                        led.setPower(0.2);
                        sleep(2000);
                    }
                    led.setPower(0);
                }
            });

            marker.setPosition(0.8); //Lock marker in position

            int count =0;
            double lasttime=0;
            while (!opModeIsActive() && !isStopRequested()){
                //stuff to do in init, but before start
                //Block for lift
                if (gamepad2.y){
                    driveLift(1);
                } else if(gamepad2.a) {
                    driveLift(-1);
                }else {
                    driveLift(0);

                }
                if (getRuntime()-lasttime > 1.7) {
                    lasttime=getRuntime();
                    GoldBlockPosition goldBlockPositioninit = findBlock();
                    telemetry.addData("Gold Block Position Detected:", goldBlockPositioninit);
                    telemetry.addData("Count:", count);
                    telemetry.update();

                    telemetry.addData("UPDATING:", count);
                    telemetry.update();
                    count++;
                }
            }
        }


        telemetry.addData("Status","Init cycle complete");
        telemetry.update();
    }

    public void turn(double degrees, Direction direction, double maxSpeed) {
        if (!opModeIsActive()) return;
        turn(degrees, direction, maxSpeed, 1, 10000);
    }


    public void turn(double degrees, Direction direction, double maxSpeed, int count, double timeout) {
        if (!opModeIsActive()) return; //Necessary because turn method is recursive
        if (direction.equals(Direction.RIGHT)) degrees *= -1; //Negative degree for turning left
        double initialHeading = getIntegratedHeading();
        double targetHeading = initialHeading + degrees; //Turns are relative to current position

        //Change mode because turn() uses motor power PID and not motor position
        motorLeftFront.setMode(RUN_USING_ENCODER);
        motorLeftBack.setMode(RUN_USING_ENCODER);
        motorRightFront.setMode(RUN_USING_ENCODER);
        motorRightBack.setMode(RUN_USING_ENCODER);

        ElapsedTime timer = new ElapsedTime();
        timer.startTime();

        //While target has not been reached, stops robot if target is overshot
        while (((degrees < 0 && getIntegratedHeading() > targetHeading) || (degrees > 0 && getIntegratedHeading() < targetHeading)) && (timer.milliseconds() < timeout) && opModeIsActive()) {
            double currentHeading = getIntegratedHeading();

            double robotSpeed = Range.clip(maxSpeed * (Math.abs(targetHeading - getIntegratedHeading()) / Math.abs(degrees)), turnSpeedMin, maxSpeed);

            motorLeftFront.setPower(degrees > 0 ? robotSpeed : -robotSpeed);
            motorLeftBack.setPower(degrees > 0 ? robotSpeed : -robotSpeed);
            motorRightFront.setPower(degrees > 0 ? -robotSpeed : robotSpeed);
            motorRightBack.setPower(degrees > 0 ? -robotSpeed : robotSpeed);

            telemetry.addData("Heading", getIntegratedHeading());
            telemetry.addData("Distance to turn: ", Math.abs(currentHeading - targetHeading));
            telemetry.addData("Target", targetHeading);
            telemetry.addData("Heading", currentHeading);
            telemetry.addData("Initial Heading", initialHeading);
            telemetry.addData("Power", robotSpeed);
            telemetry.update();
        }

        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(300);

        telemetry.addData("Distance to turn", Math.abs(getIntegratedHeading() - targetHeading));
        telemetry.addData("Direction", -1 * (int) Math.signum(degrees));
        telemetry.update();

        if (Math.abs(getIntegratedHeading() - targetHeading) > 1 && count > 0) { //If the target was significantly overshot
            //Recurse to correct turn, turning in the opposite direction
            turn(Math.abs(getIntegratedHeading() - targetHeading), direction.equals(Direction.LEFT) ? Direction.RIGHT : Direction.LEFT, .05, --count, 2000);
        }
    }

    public void move(double distance, Direction direction, double maxSpeed, boolean recurse, double timeout) {

        double initialHeading = getIntegratedHeading();
        double motorInitial = motorLeftFront.getCurrentPosition();

        //Change mode because move() uses setTargetPosition()
        motorLeftFront.setMode(RUN_TO_POSITION);
        motorLeftBack.setMode(RUN_TO_POSITION);
        motorRightFront.setMode(RUN_TO_POSITION);
        motorRightBack.setMode(RUN_TO_POSITION);

        if (direction == Direction.FORWARD) {
            distance *= ticksRatioForward;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() + distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
        } else if (direction == Direction.BACKWARD) {
            distance *= ticksRatioForward;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() - distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
        } else if (direction == Direction.LEFT) {
            distance *= ticksRatioStrafe;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
        } else {
            distance *= ticksRatioStrafe;
            motorLeftFront.setTargetPosition((int) (motorLeftFront.getCurrentPosition() - distance));
            motorLeftBack.setTargetPosition((int) (motorLeftBack.getCurrentPosition() + distance));
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
        }

        double moveSpeed = moveSpeedMin;

        motorLeftFront.setPower(moveSpeed);
        motorLeftBack.setPower(moveSpeed);
        motorRightFront.setPower(moveSpeed);
        motorRightBack.setPower(moveSpeed);

        ElapsedTime timer = new ElapsedTime();
        timer.startTime();

        while ((motorLeftFront.isBusy() && motorLeftBack.isBusy() && motorRightFront.isBusy() && motorRightBack.isBusy()) && timer.milliseconds() < timeout && opModeIsActive()) {
            //Only one encoder target must be reached

            //Trapezoidal motion profile
            if (Math.abs(motorLeftFront.getCurrentPosition() - motorInitial) < 500) {
                moveSpeed = Math.min(maxSpeed, moveSpeed + .03); //Ramp up motor speed at beginning of move
            } else if (Math.abs(motorLeftFront.getCurrentPosition() - motorLeftFront.getTargetPosition()) < 1000) { //Ramp down motor speed at end of move
                moveSpeed = Math.max(moveSpeedMin, moveSpeed - .04);
            }

            //if(direction.equals(Direction.LEFT) || direction.equals(Direction.RIGHT)) moveSpeed = .8;

            if (Math.abs(getIntegratedHeading() - initialHeading) > 2) {
                if (getIntegratedHeading() < initialHeading) { //Turn left
                    motorLeftFront.setPower(moveSpeed - .05);
                    motorLeftBack.setPower(moveSpeed - .05);
                    motorRightFront.setPower(moveSpeed + .05);
                    motorRightBack.setPower(moveSpeed + .05);
                } else { //Turn right
                    motorLeftFront.setPower(moveSpeed + .05);
                    motorLeftBack.setPower(moveSpeed + .05);
                    motorRightFront.setPower(moveSpeed - .05);
                    motorRightBack.setPower(moveSpeed - .05);
                }
            } else {
                motorLeftFront.setPower(moveSpeed);
                motorLeftBack.setPower(moveSpeed);
                motorRightFront.setPower(moveSpeed);
                motorRightBack.setPower(moveSpeed);
            }


        }

        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(400);
        //Correct if robot turned during movement
        if (Math.abs(getIntegratedHeading() - initialHeading) > 1 && recurse) {

            turn(Math.abs(getIntegratedHeading() - initialHeading), getIntegratedHeading() < initialHeading ? Direction.LEFT : Direction.RIGHT, .1);
        }

        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void move45(double distance, Diagonal diagonal, double maxSpeed) {
        move45(distance, diagonal, maxSpeed, false);

    }

    public void move45(double distance, Diagonal diagonal, double maxSpeed, boolean recurse) {
        double initialHeading = getIntegratedHeading();

        //Change mode because move() uses setTargetPosition()
        motorLeftFront.setMode(RUN_TO_POSITION);
        motorLeftBack.setMode(RUN_TO_POSITION);
        motorRightFront.setMode(RUN_TO_POSITION);
        motorRightBack.setMode(RUN_TO_POSITION);

        double moveSpeed = maxSpeed;

        distance *= ticksRatioDiagonal;
        if (diagonal == Diagonal.BL) {
            motorLeftFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
            motorLeftFront.setPower(moveSpeed);
            motorLeftBack.setPower(0);
            motorRightFront.setPower(0);
            motorRightBack.setPower(moveSpeed);
            while (motorLeftFront.isBusy() && motorRightBack.isBusy()) {

            }
        } else if (diagonal == Diagonal.BR) {
            motorLeftBack.setTargetPosition((int) (motorRightFront.getCurrentPosition() - distance));
            motorRightFront.setTargetPosition((int) (motorRightBack.getCurrentPosition() - distance));
            motorLeftFront.setPower(0);
            motorLeftBack.setPower(moveSpeed);
            motorRightFront.setPower(moveSpeed);
            motorRightBack.setPower(0);
            while (motorLeftBack.isBusy() && motorRightFront.isBusy()) {

            }
        } else if (diagonal == Diagonal.FR) {
            motorRightFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorLeftBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
            motorLeftFront.setPower(0);
            motorLeftBack.setPower(moveSpeed);
            motorRightFront.setPower(moveSpeed);
            motorRightBack.setPower(0);
            while (motorLeftBack.isBusy() && motorRightFront.isBusy()) {
                telemetry.addData("Target",motorRightFront.getTargetPosition() + "-"+motorLeftBack.getTargetPosition());
                telemetry.addData("RB Current",motorRightBack.getCurrentPosition());
                telemetry.addData("RF Current",motorRightFront.getCurrentPosition());
                telemetry.update();
            }
        } else if (diagonal == Diagonal.FL) {
            motorLeftFront.setTargetPosition((int) (motorRightFront.getCurrentPosition() + distance));
            motorRightBack.setTargetPosition((int) (motorRightBack.getCurrentPosition() + distance));
            motorLeftFront.setPower(moveSpeed);
            motorLeftBack.setPower(0);
            motorRightFront.setPower(0);
            motorRightBack.setPower(moveSpeed);
            while (motorLeftFront.isBusy() && motorRightBack.isBusy()) {

            }
        }

        //Correct if robot turned during movement
        if (Math.abs(getIntegratedHeading() - initialHeading) > 1 && recurse) {
            turn(Math.abs(getIntegratedHeading() - initialHeading), getIntegratedHeading() < initialHeading ? Direction.LEFT : Direction.RIGHT, .1);
        }

        motorLeftBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorRightBack.setPower(0);
        sleep(300);


    }

    public void driveLift(double power) {

        int red = hardwareMap.get(ColorSensor.class, "liftColorSensor").red();
        int blue = hardwareMap.get(ColorSensor.class, "liftColorSensor").blue();
        if (red < 1500 && power < 0) { //at blue, don't go down
            lift.setPower(0);
        } else if (blue < 1600 && power > 0) {
            lift.setPower(0);

        } else {
            lift.setPower(power);
        }


    }

    public double getIntegratedHeading() {
        //IMU is mounted vertically, so the Y axis is used for turning
        double currentHeading = imu.getAngularOrientation(AxesReference.EXTRINSIC, AxesOrder.XYZ, AngleUnit.DEGREES).thirdAngle;
        double deltaHeading = currentHeading - previousHeading;

        if (deltaHeading < -180) deltaHeading += 360;
        else if (deltaHeading >= 180) deltaHeading -= 360;

        integratedHeading += deltaHeading;
        previousHeading = currentHeading;

        return integratedHeading;
    }

    public void dropFromLander() {

        //int blue = hardwareMap.get(ColorSensor.class, "liftColorSensor").blue();
        time = getRuntime();
        while (hardwareMap.get(ColorSensor.class, "liftColorSensor").blue() > 1600 && (getRuntime()-time<7)) {
            telemetry.addData("Time",getRuntime()-time);
            telemetry.addData("blue",hardwareMap.get(ColorSensor.class, "liftColorSensor").blue());
            driveLift(1);
        }
        driveLift(0);

    }



    public void knockGoldBlock(GoldBlockPosition position) {

        if (position == GoldBlockPosition.LEFT) {
            move45(32, Diagonal.BL, 0.6, false);
        } else if (position == GoldBlockPosition.RIGHT) {
            move45(32, Diagonal.BR, 0.6, false);
        } else if (position == GoldBlockPosition.CENTER) {
            move(26, Direction.BACKWARD, 0.45, false, 3000);
        }
    }

    public void succThatGoldBoi(GoldBlockPosition position) {
        if (position == GoldBlockPosition.CENTER) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    intake.setPower(-1);
                }
            }).start();
            extendArm(27, -1);
            move(6, Direction.BACKWARD, 1, false, 2000);
            intake.setPower(0);
            suckInArm(27, 1);
        }
    }

    public void dropMarker() {

        multiThread.queue.offer(new TaskData(0, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                blinkin.setPattern(RevBlinkinLedDriver.BlinkinPattern.STROBE_WHITE);
            }
        }));
        multiThread.queue.offer(new TaskData(1000, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                blinkin.setPattern(defaultLEDPattern);
            }
        }));
        marker.setPosition(0);
    }

    public void succInGold() {
        intake.setPower(1);
        sleep(2000);
        intake.setPower(0);
    }

    public void waitForMotors() {
        while (opModeIsActive() && (motorLeftBack.isBusy() || motorLeftFront.isBusy() || motorRightBack.isBusy() || motorRightFront.isBusy())) {
            telemetry.addData("> ", "Waiting for motors to stop running to position");
            telemetry.update();
        }
    }

    public void startLookingForBlock() {
        detector = new GoldAlignDetector();
        detector.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        /*
        detector.useDefaults();

        // Optional Tuning
        detector.alignSize = 100; // How wide (in pixels) is the range in which the gold object will be aligned. (Represented by green bars in the preview)
        detector.alignPosOffset = 0; // How far from center frame to offset this alignment zone.
        detector.downscale = 0.4; // How much to downscale the input frames

        detector.areaScoringMethod = DogeCV.AreaScoringMethod.MAX_AREA; // Can also be PERFECT_AREA
        //detector.perfectAreaScorer.perfectArea = 10000; // if using PERFECT_AREA scoring
        detector.maxAreaScorer.weight = 0.005;

        detector.ratioScorer.weight = 5;
        detector.ratioScorer.perfectRatio = 1.0;
        */

        detector.cropTLCorner=new Point(100,100);

        detector.cropBRCorner = new Point(400,600);

        detector.enable();
    }

    public GoldBlockPosition findBlock() {
        startLookingForBlock();
        sleep(1000);//originally 2000
        if (detector.isFound()) {
            if (detector.getXPosition() < 300) {
                detector.disable();
                return GoldBlockPosition.CENTER;
            }
            detector.disable();
            return GoldBlockPosition.RIGHT;
        }
        detector.disable();
        return GoldBlockPosition.LEFT;
    }

    public void raiseToLander() {
        sleep(200);
        double runtimeTarget = getRuntime() + 6.5;
        while (getRuntime() < runtimeTarget) {
            driveLift(-1);
        }
        driveLift(0);
        sleep(200);
    }
    public void bringArmToCollectionPosition(){
        sleep(200);
        multiThread.queue.offer(new TaskData(0, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                setArmPower(-0.3);
            }
        }));
        multiThread.queue.offer(new TaskData(1500, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                armExtender.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                armExtender.setPower(1);
            }
        }));
        multiThread.queue.offer(new TaskData(2500, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                armExtender.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                armExtender.setTargetPosition(armExtender.getCurrentPosition());
            }
        }));
        multiThread.queue.offer(new TaskData(5000, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                setArmPower(0);
            }
        }));
        sleep(200);
    }

    public void strafe(int distance, double angle) {


        //RUN_USING_ENCODER is being used, so the power can be positive or negative
        motorLeftFront.setPower(-.3);
        motorLeftBack.setPower(-.5);
        motorRightFront.setPower(-.5);
        motorRightBack.setPower(-.3);

        //Target forwards or backwards is dictated by the sign of motor power variable
        motorLeftFront.setTargetPosition(motorLeftFront.getCurrentPosition() + distance * (int) Math.signum(-.3));
        motorLeftBack.setTargetPosition(motorLeftBack.getCurrentPosition() + distance * (int) Math.signum(-.5));
        motorRightFront.setTargetPosition(motorRightFront.getCurrentPosition() + distance * (int) Math.signum(-.5));
        motorRightBack.setTargetPosition(motorRightBack.getCurrentPosition() + distance * (int) Math.signum(-.3));

        //The target will be reached when the motors that have a power of 1 have reached their targets
        //For example, if moving 45 degrees, only LF and RB targets matter
        //This should still be the case even if the other wheels' power is not zero
        while (
                (
                        //|| can be used in this case because there is no RUN_TO_POSITION - the robot should not turn at the end
                        motorLeftFront.getPower() == 1 && Math.abs(motorLeftFront.getCurrentPosition() - motorLeftFront.getTargetPosition()) < ENCODER_THRESHOLD ||
                                motorLeftBack.getPower() == 1 && Math.abs(motorLeftBack.getCurrentPosition() - motorLeftBack.getTargetPosition()) < ENCODER_THRESHOLD ||
                                motorRightFront.getPower() == 1 && Math.abs(motorRightFront.getCurrentPosition() - motorRightFront.getTargetPosition()) < ENCODER_THRESHOLD ||
                                motorRightBack.getPower() == 1 && Math.abs(motorRightBack.getCurrentPosition() - motorRightBack.getTargetPosition()) < ENCODER_THRESHOLD
                ) && opModeIsActive()) {
            //Intentionally left empty
        }


    }



    public void extendArm(double dist, double power) {

        dist *= ticksRatioArm;

        armExtender.setMode(RUN_TO_POSITION);
        armExtender.setTargetPosition((int) (armExtender.getCurrentPosition() + dist));
        armExtender.setPower(power);


    }

    public void extendArmAsThread(final double dist, final double power) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                extendArm(dist, power);
            }
        }).start();
    }

    public void suckInArm(double dist, double power) {

        dist *= ticksRatioArm;

        armExtender.setMode(RUN_TO_POSITION);
        armExtender.setTargetPosition((int) (armExtender.getCurrentPosition() - dist));
        armExtender.setPower(-power);

    }

    public void suckInArmAsThread(final double dist, final double power) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                suckInArm(dist, power);
            }
        }).start();
    }

    public void setArmPower(double power) {
        //Pos power is down
        if ((armAtDump())&& !safetyOverride) {
            if (power > 0) power = 0;
        }
        if ((armAtMin())&& !safetyOverride) {
            if (power < 0) power = 0;
        }
        if (power == 0) { //Lock arm in position
            arm.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            arm.setTargetPosition(arm.getCurrentPosition());
            arm.setPower(-1);
        } else {
            arm.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            if (armPotentiometer.getVoltage()<1.6 && power>0){
                arm.setPower(power/3);
            }else {
                arm.setPower(power);
            }
        }
    }
    public boolean armAtMax(){
        return armPotentiometer.getVoltage()<ARM_MAX_POSITION;
    }
    public boolean armAtMin(){
        return armPotentiometer.getVoltage()>ARM_MIN_POSITION;
    }
    public boolean armAtDump(){
        return armPotentiometer.getVoltage()<ARM_DUMP_POSITION;
    }

    public void calibrateArm() {
        extendArm(5, -1);
        double time = getRuntime();
        while (armLimitSwitch.getValue() != 1.0 && (getRuntime() - time) < 3) {
            setArmPower(1);//We're going down, we're going down now.
            telemetry.addData("We're going down now", getRuntime() - time);
            telemetry.update();
        }
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        sleep(200);
        arm.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        sleep(200);
        arm.setTargetPosition(-850);//How many ticks to run up
        arm.setPower(-1);
        while (arm.isBusy()) {
            sleep(100);
        }
    }

    public void liftToHangingPosition() {
        multiThread.queue.offer(new TaskData(200, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                driveLift(-1);
            }
        }));
        multiThread.queue.offer(new TaskData(950, new ThreadTaskInterface() {
            @Override
            public void runTask() {
                driveLift(0);
            }
        }));
    }

    public void knockBlockWithArm(GoldBlockPosition position) {
        if (position == GoldBlockPosition.CENTER) {

            blockKnockAlgorhitm();

        } else if (position == GoldBlockPosition.RIGHT) {

            turn(33, Direction.RIGHT, .4);
            blockKnockAlgorhitm();

        } else if (position == GoldBlockPosition.LEFT) {

            turn(33, Direction.LEFT, .4);
            blockKnockAlgorhitm();

        }
    }

    public void blockKnockAlgorhitm() {
        extendArm(27, 1);
        sleep(500);
        setArmPower(.7);
        sleep(2500);
        setArmPower(-0.7);
        sleep(750);
        suckInArm(21, 1);
        setArmPower(0);
        sleep(2000);
    }

    public void retraceKnockGoldBlock(GoldBlockPosition position) {
        if (position == GoldBlockPosition.LEFT) {
            move45(31, Diagonal.FL, 0.6, true);
        } else if (position == GoldBlockPosition.RIGHT) {
            move45(31, Diagonal.FR, 0.6, true);
        } else if (position == GoldBlockPosition.CENTER) {
            move(26, Direction.FORWARD, 0.65, false, 3000);
        }
    }
    public void retraceKnockGoldBlockDepot(GoldBlockPosition position) {
        if (position == GoldBlockPosition.LEFT) {
            move45(28, Diagonal.FL, 1, false);
        } else if (position == GoldBlockPosition.RIGHT) {
            move45(31, Diagonal.FR, 0.8, true);
        } else if (position == GoldBlockPosition.CENTER) {
            move(22, Direction.FORWARD, 0.65, false, 3000);
        }
    }

    public void whoopityScoopScoopDittyWhoop(){
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setPower(-1);
        motorLeftFront.setPower(-1);
        motorRightBack.setPower(-1);
        motorRightFront.setPower(-1);
        sleep(400);
        motorLeftBack.setPower(-.75);
        motorLeftFront.setPower(-.75);
        motorRightBack.setPower(-1);
        motorRightFront.setPower(-1);
        sleep(2000);
        motorLeftBack.setPower(0);
        motorLeftFront.setPower(0);
        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
    }

    public void restOfAutoDepot(){

        moveSpeedMin = 1;



        move(10, Direction.BACKWARD, 1, false, 1000);
        turn(87, Direction.LEFT, .7);
        move(38, Direction.BACKWARD, 1, false, 3000);
        turn(45, Direction.LEFT, .7, 0, 2000);
    }

    public void restOfAuto() {

        moveSpeedMin = 1;

        move(10, Direction.BACKWARD, 1, false, 1000);
        turn(87, Direction.LEFT, .7);
        move(38, Direction.BACKWARD, 1, false, 3000);
        turn(45, Direction.LEFT, .7, 0, 2000);
    }

    public void restOfAutoForConvinence(){

        moveSpeedMin = 1;
        extendArm(14, 1);
        move(32, Direction.BACKWARD, 1, false, 4000);
        dropMarker();
        suckInArm(18, 1);
        move(57, Direction.FORWARD, 1, false, 5000);

    }

    public void raiseArm(){
        arm.setMode(RUN_TO_POSITION);
        arm.setTargetPosition(15);
        arm.setPower(-.4);
        while (arm.isBusy()){

        }
        arm.setPower(0);
    }

    public void fiveDegreeStrafe(double timeInSec){
        double startTime = getRuntime();
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        while (getRuntime()-startTime<timeInSec) {
            motorRightBack.setPower(-0.7);
            motorRightFront.setPower(-1.0);
            motorLeftFront.setPower(-0.7);
            motorLeftBack.setPower(-1.0);
        }
        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);

    }

    public void fiveDegreeStrafeForward(double timeInSec){
        double startTime = getRuntime();
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        while (getRuntime()-startTime<timeInSec) {
            motorRightBack.setPower(1);
            motorRightFront.setPower(.7);
            motorLeftFront.setPower(1);
            motorLeftBack.setPower(.7);
        }
        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);

    }

    public void fiveDegreeStrafeSlow(double timeInSec){
        double startTime = getRuntime();
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        while (getRuntime()-startTime<timeInSec) {
            motorRightBack.setPower(-.45);
            motorRightFront.setPower(-.6);
            motorLeftFront.setPower(-.45);
            motorLeftBack.setPower(-0.6);
        }
        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);

    }

    public void swoopStrafeRight(double speed, double arc, double angle, double duration){
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        double startTime = getRuntime();
        double delT = 0;
        double arc1 = 0;
        double arc2 = 0;
        while ((getRuntime() - startTime)<duration){
            delT = (getRuntime() - startTime);
            arc1 = ((-speed)/(1 + Math.pow(Math.E,-arc *(delT-(duration/2)) )))+angle;
            arc2 = ((-speed)/(1 + Math.pow(Math.E,+arc *(delT-(duration/2)) )))+angle;

            motorRightBack.setPower(arc1);
            motorRightFront.setPower(arc2);
            motorLeftFront.setPower(arc1);
            motorLeftBack.setPower(arc2);
            telemetry.addData("arcs",arc1+"-"+arc2+"-");
            telemetry.addData("time",getRuntime());
            telemetry.addData("delt",delT);
            telemetry.addData("mlp",motorLeftBack.getPower());
            telemetry.update();
        }

        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);


    }

    public void swoopStrafeLeft(double speed, double arc, double angle, double duration){
        motorLeftFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorLeftBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightFront.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motorRightBack.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        double startTime = getRuntime();
        double delT = 0;
        double arc1 = 0;
        double arc2 = 0;
        while ((getRuntime() - startTime)<duration){
            delT = (getRuntime() - startTime);
            arc1 = ((-speed)/(1 + Math.pow(Math.E,-arc *(delT-(duration/2)) )))+angle;
            arc2 = ((-speed)/(1 + Math.pow(Math.E,+arc *(delT-(duration/2)) )))+angle;

            motorRightBack.setPower(arc2);
            motorRightFront.setPower(arc1);
            motorLeftFront.setPower(arc2);
            motorLeftBack.setPower(arc1);
            telemetry.addData("arcs",arc1+"-"+arc2+"-");
            telemetry.addData("time",getRuntime());
            telemetry.addData("delt",delT);
            telemetry.addData("mlp",motorLeftBack.getPower());
            telemetry.update();
        }

        motorRightBack.setPower(0);
        motorRightFront.setPower(0);
        motorLeftFront.setPower(0);
        motorLeftBack.setPower(0);


    }


}